package com.kolovsky.app

import com.typesafe.config.{Config, ConfigFactory}

/**
  * Created by kolovsky on 6.6.17.
  */
object AppConf {
  val config: Config = ConfigFactory.load("app.conf")
  val db_url: String = config.getString("app.database.url")
}
