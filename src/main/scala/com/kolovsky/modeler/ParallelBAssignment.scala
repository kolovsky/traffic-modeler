package com.kolovsky.modeler

import com.kolovsky.app.AppConf
import com.kolovsky.graph.Graph
import com.kolovsky.java.TmParallelBStaticTrafficAssignment
import com.kolovsky.modeler.Types.ROWODM
import com.kolovsky.java.TmCallbackInvokable
import org.apache.spark.broadcast.Broadcast

/**
  * Computes static traffic assignment (user equilibrium) using the parallel B algorithm
  * @param g graph representing the road traffic network
  * @param modelName the name of the model represented by the graph
  * @param initialEdgesCosts initial costs of the graph edges
  * @param edgesCapacities capacities of the graph edges
  * @param maximalIterationsCount maximal number of iterations of the B static traffic assignment
  * @param epsilon maximal desired epsilon of the B static traffic assignment
  * @param callbackMethod the method, which is invoked in each iteration to provide intermediate results
  * @param workingThreadsCount the number of working threads
  * @param debug determines whether the debugging mode is on or off
  * @author Tomas Potuzak
  */
class ParallelBAssignment(
                         g: Broadcast[Graph],
                         modelName: String,
                         initialEdgesCosts: Array[Double],
                         edgesCapacities: Array[Double],
                         maximalIterationsCount: Int,
                         epsilon: Double,
                         callbackMethod: Array[Double] => Unit = _ => (),
                         workingThreadsCount: Int,
                         debug: Boolean = false) extends EquilibriumAssignment(g, debug) with Serializable with TmCallbackInvokable {

  override def run(odm: ROWODM): Array[Double] = {
    startTime = System.currentTimeMillis()
    val parallelBStaticTrafficAssignment = new TmParallelBStaticTrafficAssignment(AppConf.db_url, modelName, initialEdgesCosts, edgesCapacities, maximalIterationsCount, epsilon, workingThreadsCount, this);
    parallelBStaticTrafficAssignment.calculateEdgesFlowsAssignment();
    println("Execution time " + (System.currentTimeMillis() - startTime) / 1000.0 + " s")

    parallelBStaticTrafficAssignment.getAssignedEdgesFlows();
  }

  override def invokeCallback(data: Array[Double]): Unit = {
    callbackMethod(data)
  }
}
