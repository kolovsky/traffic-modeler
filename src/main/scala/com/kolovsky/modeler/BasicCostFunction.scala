package com.kolovsky.modeler

/**
  * Created by kolovsky on 29.5.17.
  */
class BasicCostFunction(alpha: Double, beta: Double) extends CostFunction with Serializable{
  override def cost(traffic: Double, capacity: Double, initCost: Double): Double = {
    val out = initCost * (1 + alpha * math.pow(traffic/capacity, beta))
    if ((out.isNaN || out.isInfinity) && !initCost.isInfinity){
      throw new Exception("NanInf: "+traffic+", "+capacity+", "+initCost)
    }
    out
  }

  override def derivation(traffic: Double, capacity: Double, initCost: Double): Double = {
    val out = alpha*beta* initCost * math.pow(traffic, beta - 1) / math.pow(capacity,beta)
    if ((out.isNaN || out.isInfinity) && !initCost.isInfinity){
      throw new Exception("NanInf: "+traffic+", "+capacity+", "+initCost)
    }
    out
  }

  override def integral(traffic: Double, capacity: Double, initCost: Double): Double = {
    val out = initCost*( (alpha*traffic*math.pow(traffic/capacity, beta)) / (beta + 1) + traffic)
    if ((out.isNaN || out.isInfinity) && !initCost.isInfinity){
      throw new Exception("NanInf: "+traffic+", "+capacity+", "+initCost)
    }
    out
  }
}
