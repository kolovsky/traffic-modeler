package com.kolovsky.java;

import java.util.ArrayList;
import java.util.List;

/**
 * A node of the graph representing the road traffic network
 * @author Tomas Potuzak
 */
public class TmNode implements Comparable<TmNode> {
    /** The ID of this graph node */
    protected int id;
    /** The index of this graph node in the list of all nodes */
    protected int index;
    /** The graph edges incoming to this graph node */
    protected List<TmEdge> incomingEdges;
    /** The graph edges outgoing from this graph node */
    protected List<TmEdge> outgoingEdges;

    /**
     * Creates a new graph node with specified ID
     * @param id the ID of the graph node
     */
    public TmNode(int id) {
        this.id = id;

        incomingEdges = new ArrayList<>();
        outgoingEdges = new ArrayList<>();
    }

    //Getters and setters

    /**
     * Returns the ID of this graph node
     * @return the ID of this graph node
     */
    public int getId() {
        return id;
    }



    /**
     * Sets the ID of this graph node
     * @param id the new ID of this graph node
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Returns the index of this graph node
     * @return the index of this graph node
     */
    public int getIndex() {
        return index;
    }

    /**
     * Sets the index of this graph node
     * @param index the new index of this graph node
     */
    public void setIndex(int index) {
        this.index = index;
    }

    /**
     * Adds the specified incoming edge to this graph node
     * @param incomingEdge the incoming edge to be added to this graph node
     */
    public void addIncomingEdge(TmEdge incomingEdge) {
        incomingEdges.add(incomingEdge);
    }

    /**
     * Returns the edges incoming to this graph node
     * @return the edges incoming to this graph node
     */
    public List<TmEdge> getIncomingEdges() {
        return incomingEdges;
    }

    /**
     * Add the specified outgoing edge to this graph node
     * @param outgoingEdge the outgoing edge to be added to this graph node
     */
    public void addOutgoingEdge(TmEdge outgoingEdge) {
        outgoingEdges.add(outgoingEdge);
    }

    /**
     * Returns the edges outgoing from this graph node
     * @return the edges outgoing from this graph node
     */
    public List<TmEdge> getOutgoingEdges() {
        return outgoingEdges;
    }

    //Inherited methods

    @Override
    public int compareTo(TmNode other) {
        if (id < other.id) {
            return TmComparisonUtils.THIS_LOWER;
        }
        else if (id > other.id) {
            return TmComparisonUtils.THIS_HIGHER;
        }
        else {
            return TmComparisonUtils.EQUAL;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof TmNode) {
            TmNode other = (TmNode) o;

            if (id == other.id) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "Node ID: " + id + ", index: " + index;
    }
}