package com.kolovsky.java;

/**
 * A bush (an acyclic oriented graph) for B assignment
 * @author Tomas Potuzak
 */
public class TmBBush {
    /** The origin serving as root node of the bush */
    protected TmZone origin;
    /** Determines which edges of the graph are in the bush */
    protected boolean[] edgesMask;
    /** The traffic flows in individual edges */
    protected double[] edgesFlows;

    /**
     * Creates a new bush with specified origin and number of edges
     * @param origin the origin serving as root node of the bush
     * @param edgesCount the number of edges
     */
    public TmBBush(TmZone origin, int edgesCount) {
        this.origin = origin;

        edgesMask = new boolean[edgesCount];
        edgesFlows = new double[edgesCount];
    }

    /**
     * Returns the traffic flow in specified edge
     * @param edgeIndex the index of the edge
     * @return the traffic flow in specified edge
     */
    public double getEdgeFlow(int edgeIndex) {
        return edgesFlows[edgeIndex];
    }

    /**
     * Sets the traffic flow in specified edge
     * @param edgeIndex the index of the edge
     * @param edgeFlow the new traffic flow, which shall be set to the edge
     */
    public void setEdgeFlow(int edgeIndex, double edgeFlow) {
        edgesFlows[edgeIndex] = edgeFlow;
    }

    /**
     * Increments the traffic flow in specified edge
     * @param edgeIndex the index of the edge
     * @param edgeIncrement the increment, which shall be added to the edge
     */
    public void incrementEdgeFlow(int edgeIndex, double edgeIncrement) {
        edgesFlows[edgeIndex] += edgeIncrement;
    }

    /**
     * Returns <code>true</code> if the specified edge is in this bush. Returns <code>false</code> otherwise
     * @param edgeIndex the index of the edge
     * @return <code>true</code> if the specified edge is in this bush. Returns <code>false</code> otherwise
     */
    public boolean getEdgeMask(int edgeIndex) {
        return edgesMask[edgeIndex];
    }

    /**
     * Sets whether a specified edge is in this bush
     * @param edgeIndex the index of the edge
     * @param edgeMask indicates whether this edge is in this bush or not
     */
    public void setEdgeMask(int edgeIndex, boolean edgeMask) {
        edgesMask[edgeIndex] = edgeMask;
    }

    //Getters and setters

    /**
     * Returns the origin of this bush
     * @return the origin of this bush
     */
    public TmZone getOrigin() {
        return origin;
    }

    /**
     * Returns the edges, which are in this bush
     * @return the edge, which are in this bush
     */
    public boolean[] getEdgesMask() {
        return edgesMask;
    }

    /**
     * Returns the flows of the edges in this bush
     * @return the edgesFlows
     */
    public double[] getEdgesFlows() {
        return edgesFlows;
    }

    @Override
    public String toString() {
        return "Bush (origin node index " + origin.getNode().getId() + ")";
    }
}