package com.kolovsky.java;

/**
 * A pair representing flows from an origin node a destination node (i.e., row of the origin destination matrix)
 * @author Tomas Potuzak
 */
public class TmOriginDestinationPair implements Comparable<TmOriginDestinationPair> {
    /** The origin, from which the flow is moving to the destination */
    protected TmZone origin;
    /** The destination, to which the flow is moving from the origin */
    protected TmZone destination;
    /** The flow representing the amount of vehicles moving from the origin to the destination */
    protected double flow;

    /**
     * Creates a new origin-destination pair (i.e, row of the origin-destination matrix) with specified origin, destination, and flow
     * @param origin the origin, from which the flow is moving to the destination
     * @param destination the destination, to which the flow is moving from the origin
     * @param flow the flow representing the amount of vehicles moving from the origin to the destination
     */
    public TmOriginDestinationPair(TmZone origin, TmZone destination, double flow) {
        this.origin = origin;
        this.destination = destination;
        this.flow = flow;
    }

    //Getters and setters

    /**
     * Returns the origin of this origin-destination pair
     * @return the origin of this origin-destination pair
     */
    public TmZone getOrigin() {
        return origin;
    }

    /**
     * Sets the origin of this origin-destination pair
     * @param origin the new origin of this origin-destination pair
     */
    public void setOrigin(TmZone origin) {
        this.origin = origin;
    }

    /**
     * Returns the destination of this origin-destination pair
     * @return the destination of this origin-destination pair
     */
    public TmZone getDestination() {
        return destination;
    }

    /**
     * Sets the destination of this origin-destination pair
     * @param destination the new destination of this origin-destination pair
     */
    public void setDestination(TmZone destination) {
        this.destination = destination;
    }

    /**
     * Returns the flow of this origin-destination pair
     * @return the flow of this origin-destination pair
     */
    public double getFlow() {
        return flow;
    }

    /**
     * Sets the flow of this origin-destination pair
     * @param flow the new flow of this origin-destination pair
     */
    public void setFlow(double flow) {
        this.flow = flow;
    }

    //Inherited methods

    @Override
    public int compareTo(TmOriginDestinationPair other) {
        return origin.compareTo(other.origin);
    }

    @Override
    public String toString() {
        return origin + " -> " + destination + ", flow: " + flow;
    }
}