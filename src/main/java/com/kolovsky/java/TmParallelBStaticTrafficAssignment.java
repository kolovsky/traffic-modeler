package com.kolovsky.java;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * A class with parallel B static traffic assignment. The instance of this class have methods, which shall be invoked by working threads of the parallel computation
 * @author Tomas Potuzak
 */
public class TmParallelBStaticTrafficAssignment {
    /** Default maximal number of Newton method iterations for shifting of flows between minimal and maximal path */
    public static final int DEFAULT_NEWTON_METHOD_ITERATIONS_COUNT = 100;
    /** Connection to the database with the graph and the origin-destination matrix */
    protected TmDbConnector dbConnector;
    /** Maximal number of iterations of the B static traffic assignment */
    protected int iterationsCount;
    /** Maximal desired epsilon of the B static traffic assignment */
    protected double epsilon;
    /** Currently performed iteration of the B static traffic assignment */
    protected int currentIteration;
    /** Relative gap calculated using the all-or-nothing assignment with edges costs determined by the B assignment. The computation is finished when the relative gap is lower than epsilon  */
    protected double relativeGap;
    /** Cost function used for calculation of the costs of individual edges and also for integral and derivation of the costs */
    protected TmBCostFunction costFunction;
    /** The graph representing the road traffic network */
    protected TmGraph graph;
    /** The origin-destination matrix representing the flows of vehicles from origin nodes to destination nodes for the road traffic network */
    protected TmOriginDestinationMatrix originDestinationMatrix;
    /** The bushes (acyclic oriented graphs) used for the B assignment and created from the graph representing the road traffic network */
    protected TmBBush[] bushes;
    /** Minimal cost trees created from the bushed used for the B assignment */
    protected TmBTree[] minimalCostTrees;
    /** Maximal cost trees created from the bushed used for the B assignment */
    protected TmBTree[] maximalCostTrees;
    /** Initial costs of the edges of the graph */
    protected double[] initialEdgesCosts;
    /** Capacities of the edges of the graph */
    protected double[] edgesCapacities;
    /** Costs of the edges of the graph */
    protected double[] edgesCosts;
    /** Flows assigned to the edges of the graph during the computation. They are returned using the callback method as the intermediate results */
    protected double[] edgesFlows;
    /** Final flows assigned to the edges of the graph once the computation is finished */
    protected double[] assignedEdgesFlows;
    /** Flows additions calculated by individual working threads during shifting of the flows between minimal and maximal flow paths */
    protected double[][] edgesFlowsAdditions;
    /** The values of the convergence during particular iterations of the computation */
    protected List<Double> convergenceHistory;
    /** The maximal value of the convergence during the entire computation. It is used for calculation of the relative gap. */
    protected double convergenceMaximum;
    /** The working threads for the parallel computation of the B static traffic assignment */
    protected TmParallelBWorkingThread[] workingThreads;
    /** Parts of the objective function results calculated by the working threads */
    protected double[] objectiveFunctionParts;
    /** The objective function utilized for the calculation of the relative gap */
    protected double objectiveFunction;
    /** Parallel all-or-nothing static traffic assignment utilized for the calculation of the relative gap using the cost of the edges determined by the B static traffic assignment */
    protected TmParallelAllOrNothingStaticTrafficAssignment parallelAllOrNothingStaticTrafficAssignment;
    /** Reference to an instance, which is used to perform callback with intermediate results during each iteration */
    protected TmCallbackInvokable callbackInvokable;

    /**
     * Creates new instance for the parallel B static traffic assignment
     * @param dbUrl the database URL used for to create the database connection for reading of the graph and the origin-destination matrix
     * @param modelName the name of the model represented by the graph
     * @param initialEdgesCosts initial costs of the edges of the graph
     * @param edgesCapacities capacities of the edges of the graph
     * @param iterationsCount maximal number of iterations of the B static traffic assignment
     * @param epsilon maximal desired epsilon of the B static traffic assignment
     * @param workingThreadsCount the number of working threads, which shall be created to perform the parallel computation
     * @param callbackInvokable reference to an instance, which is used to perform callback with intermediate results during each iteration
     * @throws ClassNotFoundException if the database driver is not found
     * @throws SQLException if there was a problem with establishing the connection (e.g., invalid user of password) or reading the graph of the origin-destination matrix
     */
    public TmParallelBStaticTrafficAssignment(String dbUrl, String modelName, double[] initialEdgesCosts, double[] edgesCapacities, int iterationsCount, double epsilon, int workingThreadsCount, TmCallbackInvokable callbackInvokable) throws ClassNotFoundException, SQLException {
        this.initialEdgesCosts = initialEdgesCosts;
        this.edgesCapacities = edgesCapacities;
        this.iterationsCount = iterationsCount;
        this.epsilon = epsilon;
        this.callbackInvokable = callbackInvokable;

        dbConnector = new TmDbConnector(dbUrl);
        dbConnector.createConnection();
        graph = dbConnector.readGraph(modelName);
        originDestinationMatrix = dbConnector.readOriginDestinationMatrix(modelName, graph);

        costFunction = new TmBCostFunction();

        TmEdge edge = null; //Store changes of capacities and costs to graph
        for (int i = 0; i < graph.getEdges().size(); i++) {
            edge = graph.getEdges().get(i);
            edge.setCost(initialEdgesCosts[i]);
            edge.setCapacity(edgesCapacities[i]);
        }
        assignedEdgesFlows = new double[initialEdgesCosts.length];

        convergenceHistory = new ArrayList<>();
        convergenceMaximum = Double.NEGATIVE_INFINITY;
        workingThreads = new TmParallelBWorkingThread[workingThreadsCount];
        objectiveFunctionParts = new double[workingThreadsCount];
        parallelAllOrNothingStaticTrafficAssignment = new TmParallelAllOrNothingStaticTrafficAssignment(graph, originDestinationMatrix, initialEdgesCosts, workingThreadsCount);
    }

    /**
     * Performs the entire B static traffic assignment computation
     */
    public void calculateEdgesFlowsAssignment() {
        long startTime = System.currentTimeMillis();
        calculateEdgesFlowsAssignmentParallel();
        long endTime = System.currentTimeMillis();

        System.out.println("Parallel B Assigment computation time is " + (endTime - startTime) + " ms.");
    }

    /**
     * Performs the entire B static traffic assignment computation in a parallel manner using working threads
     */
    protected void calculateEdgesFlowsAssignmentParallel() {
        relativeGap = Double.POSITIVE_INFINITY;
        currentIteration = 0;
        edgesCosts = new double[initialEdgesCosts.length];
        edgesFlows = new double[initialEdgesCosts.length];
        bushes = new TmBBush[originDestinationMatrix.getOrigins().length];
        minimalCostTrees = new TmBTree[bushes.length];
        maximalCostTrees = new TmBTree[bushes.length];
        edgesFlowsAdditions = new double[bushes.length][initialEdgesCosts.length];

        System.out.println("Origins count " + originDestinationMatrix.getOrigins().length + ", pairs " + originDestinationMatrix.getOriginDestinationPairs().size());

        int[][] edgesIndices = TmLoadDivider.divideLoad(workingThreads.length, initialEdgesCosts.length);
        int[][] bushesIndices = TmLoadDivider.divideLoad(workingThreads.length, bushes.length);
        TmCyclicBarrier barrier = new TmCyclicBarrier(workingThreads.length);
        for (int i = 0; i < workingThreads.length; i++) {
            workingThreads[i] = new TmParallelBWorkingThread(i, this, barrier, bushesIndices[0][i], bushesIndices[1][i], edgesIndices[0][i], edgesIndices[1][i]);
        }
        for (int i = 0; i < workingThreads.length; i++) {
            workingThreads[i].start();
        }

        while (workingThreads[0].isAlive()) {
            try {
                workingThreads[0].join();
            }
            catch (InterruptedException ex) {
                //No action
            }
        }

        assignedEdgesFlows = edgesFlows;
    }

    /**
     * Creates the bushes with root nodes in all origins from the start (inclusive) to end (exclusive) indices and fills their edges with initial flows
     * This method shall be invoked by the working threads
     * @param startIndex the start index of the origin/bush (inclusive)
     * @param endIndex the end index of the origin/bush (exclusive)
     */
    protected void initializeBushes(int startIndex, int endIndex) {
        for (int i = startIndex; i < endIndex; i++) {
            bushes[i] = graph.getBush(originDestinationMatrix.getOrigins()[i], initialEdgesCosts);
            initializeBushFlows(bushes[i]);
        }
    }

    /**
     * Calculates total flows of the edges between the start (inclusive) and the end (exclusive) indices as the sum from all bushes
     * This method shall be invoked by the working threads
     * @param startIndex the start index of the edges (inclusive)
     * @param endIndex the end index of the edges (exclusive)
     */
    protected void calculateEdgesFlows(int startIndex, int endIndex) {
        for (TmBBush bush: bushes) {
            for (int i = startIndex; i < endIndex; i++) {
                edgesFlows[i] += bush.getEdgeFlow(i);
            }
        }
    }

    /**
     * Calculates costs of the edges between the start (inclusive) and the end (exclusive) indices
     * This method shall be invoked by the working threads
     * @param startIndex the start index of the edges (inclusive)
     * @param endIndex the end index of the edges (exclusive)
     */
    protected void calculateEdgesCosts(int startIndex, int endIndex) {
        for (int i = startIndex; i < endIndex; i++) {
            edgesCosts[i] = costFunction.calculateCost(edgesFlows[i], graph.getEdges().get(i).getCapacity(), initialEdgesCosts[i]);
        }
    }

    /**
     * Changes the bushes between the start (inclusive) and the end (exclusive) indices by adding some edges to the bush
     * This method shall be invoked by the working threads
     * @param startIndex the start index of the bush (inclusive)
     * @param endIndex the end index of the bush (exclusive)
     */
    protected void improveBushes(int startIndex, int endIndex) {
        for (int i = startIndex; i < endIndex; i++) {
            improveBush(bushes[i], edgesCosts);
        }
    }

    /**
     * Finds and stores minimal and maximal cost trees for the bushes between the start (inclusive) and the end (exclusive) indices
     * This method shall be invoked by the working threads
     * @param startIndex the start index of the bush (inclusive)
     * @param endIndex the end index of the bush (exclusive)
     */
    protected void findCostTrees(int startIndex, int endIndex) {
        TmBTree[] costTrees = null;

        for (int i = startIndex; i < endIndex; i++) {
            costTrees = graph.findMinimalMaximalCostTrees(bushes[i], edgesCosts, false);
            minimalCostTrees[i] = costTrees[TmGraph.MINIMAL_COST_TREE];
            maximalCostTrees[i] = costTrees[TmGraph.MAXIMAL_COST_TREE];
        }
    }

    /**
     * Shifts flows from the edges on the maximal flow paths and to the edges on the minimal flow path
     * This method shall be invoked by the control thread
     */
    protected void shiftEdgesFlows() {
        TmPairElement<TmEdge[], TmEdge[]> differentPathSegments = null;

        for (int i = 0; i < bushes.length; i++) {
            for (TmNode node: graph.getNodes()) {
                if (minimalCostTrees[i].getPreviousEdge(node.getIndex()) != null && maximalCostTrees[i].getPreviousEdge(node.getIndex()) != null) {
                    if (minimalCostTrees[i].getPreviousEdge(node.getIndex()) != maximalCostTrees[i].getPreviousEdge(node.getIndex())) {
                        differentPathSegments = findDifferentPathSegments(node, minimalCostTrees[i], maximalCostTrees[i]);

                        if (differentPathSegments.getFirst().length > 0 && differentPathSegments.getSecond().length > 0) {
                            shiftEdgesFlows(bushes[i], differentPathSegments.getFirst(), differentPathSegments.getSecond());
                        }
                    }
                }
            }
        }
    }

    /**
     * Shifts flows from the edges on the maximal flow paths and to the edges on the minimal flow path from the bushes between the start (inclusive) and the end (exclusive) indices
     * This method shall be invoked by the working threads
     * @param startIndex the start index of the bush (inclusive)
     * @param endIndex the end index of the bush (exclusive)
     */
    //EXPERIMENT PARALLEL SHIFT EDGES FLOWS
    protected void shiftEdgesFlowsParallel(int startIndex, int endIndex) {
        TmPairElement<TmEdge[], TmEdge[]> differentPathSegments = null;

        for (int i = startIndex; i < endIndex; i++) {
            for (TmNode node: graph.getNodes()) {
                if (minimalCostTrees[i].getPreviousEdge(node.getIndex()) != null && maximalCostTrees[i].getPreviousEdge(node.getIndex()) != null) {
                    if (minimalCostTrees[i].getPreviousEdge(node.getIndex()) != maximalCostTrees[i].getPreviousEdge(node.getIndex())) {
                        differentPathSegments = findDifferentPathSegments(node, minimalCostTrees[i], maximalCostTrees[i]);

                        if (differentPathSegments.getFirst().length > 0 && differentPathSegments.getSecond().length > 0) {
                            shiftEdgesFlows(i, differentPathSegments.getFirst(), differentPathSegments.getSecond());
                        }
                    }
                }
            }
        }
    }

    /**
     * Sums the shifted flows and assign them to the bushes and edges flows
     * This method shall be invoked by the control thread
     */
    //EXPERIMENT PARALLEL SHIFT EDGES FLOWS
    protected void shiftEdgesFlowsGather() {
        for (int i = 0; i < edgesFlowsAdditions.length; i++) {
            for (int j = 0; j < edgesFlowsAdditions[i].length; j++) {
                bushes[i].incrementEdgeFlow(j, edgesFlowsAdditions[i][j]);
                edgesFlows[j] += edgesFlowsAdditions[i][j];
            }
        }
    }

    /**
     * Removes unused edges from the bushes between the start (inclusive) and the end (exclusive) indices
     * This method shall be invoked by the working threads
     * @param startIndex the start index of the bush (inclusive)
     * @param endIndex the end index of the bush (exclusive)
     */
    protected void removeUnusedEdges(int startIndex, int endIndex) {
        for (int i = startIndex; i < endIndex; i++) {
            removeUnusedEdges(bushes[i], minimalCostTrees[i]);
        }
    }

    /**
     * Calculates the objective function (which is then used for the calculation of the relative gap) for the edges between the start (inclusive) and the end (exclusive) indices
     * This method shall be invoked by the working thread with the specified index
     * @param threadIndex threadIndex index of the working thread, which invoked this method
     * @param startIndex the start index of the edge (inclusive)
     * @param endIndex the start index of the edge (exclusive)
     */
    protected void calculateObjectiveFunctionParallel(int threadIndex, int startIndex, int endIndex) {
        objectiveFunctionParts[threadIndex] = 0.0;
        for (int i = startIndex; i < endIndex; i++) {
            objectiveFunctionParts[threadIndex] += costFunction.calculateIntegral(edgesFlows[i], graph.getEdges().get(i).getCapacity(), initialEdgesCosts[i]);
        }
    }

    /**
     * Sums the precalculated parts of the objective function and sets the current edges costs to the all-or-nothing static traffic assignment
     * This method shall be invoked by the control thread
     */
    protected void calculateObjectiveFunctionGather() {
        objectiveFunction = 0.0;
        for (int i = 0; i < objectiveFunctionParts.length; i++) {
            objectiveFunction += objectiveFunctionParts[i];
        }
        parallelAllOrNothingStaticTrafficAssignment.setInitialEdgesCosts(edgesCosts);
    }

    /**
     * Adds flows from the origins to the graph for the origins between the start (inclusive) and the end (exclusive) indices using the all-or-nothing static traffic assignment wirh the current edges cost from the B static traffic assignment
     * @param threadIndex threadIndex index of the working thread, which invoked this method
     * @param startIndex the start index of the origin (inclusive)
     * @param endIndex the end index of the origin (exclusive)
     */
    protected void calculateRelativeGapParallel(int threadIndex, int startIndex, int endIndex) {
        parallelAllOrNothingStaticTrafficAssignment.addFlowsFromOrigins(threadIndex, startIndex, endIndex);
    }

    /**
     * Utilizes the assigned flows from all origins to the graph and calculated the relative gap
     * This method shall be invoked by the control thread
     */
    protected void calculateRelativeGapGather() {
        parallelAllOrNothingStaticTrafficAssignment.gatherEdgesFlows();
        double[] aonEdgesFlows = parallelAllOrNothingStaticTrafficAssignment.getAssignedEdgesFlows();
        double gap = 0.0;

        for (int i = 0; i < edgesFlows.length; i++) {
            gap += edgesCosts[i] * (aonEdgesFlows[i] - edgesFlows[i]);
        }

        double convergenceValue = objectiveFunction + gap;
        convergenceHistory.add(convergenceValue);
        if (convergenceValue > convergenceMaximum) {
            convergenceMaximum = convergenceValue;
        }

        relativeGap =  - gap / Math.abs(convergenceMaximum);
        System.out.println("Relative gap " + relativeGap);

        currentIteration++;
    }

    /**
     * Loads the edges of the bush with the flows based on the minimal cost tree
     * @param bush the bush, whose edges shall be loaded by the flows
     */
    protected void initializeBushFlows(TmBBush bush) {
        TmBTree minimalCostTree = graph.findMinimalMaximalCostTrees(bush, initialEdgesCosts, false)[TmGraph.MINIMAL_COST_TREE];
        TmOriginDestinationPair originDestinationPair = null;
        TmEdge[] path = null;

        for (int i = 0; i < originDestinationMatrix.getOriginDestinationsCount(bush.getOrigin().getId()); i++) {
            originDestinationPair = originDestinationMatrix.getOriginDestinationPair(bush.getOrigin().getId(), i);
            path = minimalCostTree.findPathToOrigin(originDestinationPair.getDestination().getNode().getIndex());

            for (int j = 0; j < path.length; j++) {
                bush.incrementEdgeFlow(path[j].getIndex(), originDestinationPair.getFlow());
            }
        }
    }

    /**
     * Changes the bush by adding some edges to the bush
     * @param bush the bush, whose edges shall be changed
     * @param edgesCosts the edges costs of the graph
     */
    protected void improveBush(TmBBush bush, double[] edgesCosts) {
        TmBTree maximalCostTree = graph.findMinimalMaximalCostTrees(bush, edgesCosts, true)[TmGraph.MAXIMAL_COST_TREE];

        TmEdge edge = null;
        for (int i = 0; i < graph.getEdges().size(); i++) {
            edge = graph.getEdges().get(i);
            if (Double.isFinite(maximalCostTree.getDistance(edge.getStartNode().getIndex())) && Double.isFinite(maximalCostTree.getDistance(edge.getEndNode().getIndex()))) {
                if (maximalCostTree.getDistance(edge.getStartNode().getIndex()) < maximalCostTree.getDistance(edge.getEndNode().getIndex())) {
                    bush.setEdgeMask(i, true);
                }
            }
        }
    }

    /**
     * Removes unused edges of the bush after the improvement of the bush using the specified cost tree
     * @param bush the bush, whose edges shall be removed
     * @param minimalCostTree the minimal cost tree, which shall be used for the edge removal
     */
    protected void removeUnusedEdges(TmBBush bush, TmBTree minimalCostTree) {
        for (int i = 0; i < bush.getEdgesMask().length; i++) {
            if (bush.getEdgeFlow(i) <= 0.0) {
                bush.setEdgeMask(i, false);
            }
        }

        TmEdge edge = null;
        for (int i = 0; i < graph.getNodes().size(); i++) {
            edge = minimalCostTree.getPreviousEdge(graph.getNodes().get(i).getIndex());
            while (edge != null) {
                bush.setEdgeMask(edge.getIndex(), true);
                edge = minimalCostTree.getPreviousEdge(edge.getStartNode().getIndex());
            }
        }
    }

    /**
     * Finds and returns the different path segments of the minimal flow path and maximal flow path from the specified node to the origin
     * @param node the node, from which both paths leading to the origin
     * @param minimalCostTree minimal cost tree used for the finding of the different path segments
     * @param maximalCostTree maximal cost tree used for the finding of the different path segments
     * @return
     */
    protected TmPairElement<TmEdge[], TmEdge[]> findDifferentPathSegments(TmNode node, TmBTree minimalCostTree, TmBTree maximalCostTree) {
        List<TmEdge> minimalPathSegmentList = new ArrayList<>();
        List<TmEdge> maximalPathSegmentList = new ArrayList<>();
        TmEdge minimalPathEdge = minimalCostTree.getPreviousEdge(node.getIndex());
        TmEdge maximalPathEdge = maximalCostTree.getPreviousEdge(node.getIndex());
        int[] minimalPathEdgesMask = new int[graph.getEdges().size()];
        int minimalPathEdgeIndex = 1;

        while (minimalPathEdge != null) {
            minimalPathSegmentList.add(minimalPathEdge);
            minimalPathEdgesMask[minimalPathEdge.getIndex()] = minimalPathEdgeIndex;
            minimalPathEdge = minimalCostTree.getPreviousEdge(minimalPathEdge.getStartNode().getIndex());
            minimalPathEdgeIndex++;
        }

        minimalPathEdgeIndex = 0;
        while (maximalPathEdge != null) {
            if (minimalPathEdgesMask[maximalPathEdge.getIndex()] > 0) {
                minimalPathEdgeIndex = minimalPathEdgesMask[maximalPathEdge.getIndex()];
                break;
            }
            maximalPathSegmentList.add(maximalPathEdge);
            maximalPathEdge = maximalCostTree.getPreviousEdge(maximalPathEdge.getStartNode().getIndex());
        }

        TmEdge[] minimalPathSegment = new TmEdge[(minimalPathEdgeIndex > 0) ? minimalPathEdgeIndex - 1 : minimalPathSegmentList.size()];
        for (int i = 0; i < minimalPathSegment.length; i++) {
            minimalPathSegment[i] = minimalPathSegmentList.get(minimalPathSegment.length - i - 1);
        }
        TmEdge[] maximalPathSegment = new TmEdge[maximalPathSegmentList.size()];
        for (int i = 0; i < maximalPathSegment.length; i++) {
            maximalPathSegment[i] = maximalPathSegmentList.get(maximalPathSegment.length - i - 1);
        }
        TmPairElement<TmEdge[], TmEdge[]> differentPathSegments = new TmPairElement<>(minimalPathSegment, maximalPathSegment);
        return differentPathSegments;
    }

    /**
     * Shifts flows between the minimal flow path and the maximal flow path of the specified bush using the Newton method. The changes in the flows are stored to the bush and to the edges flows of the entire graph
     * Utilized for sequential shift of edges flows
     * @param bush the bush, in which the shift of flows shall be performed
     * @param minimalPath minimal path, to which the flows shall be shifted
     * @param maximalPath maximal path, from which the flows shall be shifted
     */
    protected void shiftEdgesFlows(TmBBush bush, TmEdge[] minimalPath, TmEdge[] maximalPath) {
        double[] edgesFlowsAdditions = new double[edgesFlows.length];

        double maximalDx = bush.getEdgeFlow(maximalPath[0].getIndex());
        for (TmEdge edge: maximalPath) {
            if (bush.getEdgeFlow(edge.getIndex()) < maximalDx) {
                maximalDx = bush.getEdgeFlow(edge.getIndex());
            }
        }
        if (maximalDx > 0) {
            double minimalPathCost = 0.0;
            double maximalPathCost = 0.0;
            double minimalPathDerivation = 0.0;
            double maximalPathDerivation = 0.0;
            double dx = 0.0;
            int currentIteration = 0;

            while (currentIteration < DEFAULT_NEWTON_METHOD_ITERATIONS_COUNT) {
                minimalPathCost = 0.0;
                minimalPathDerivation = 0.0;
                for (TmEdge edge: minimalPath) {
                    minimalPathCost += costFunction.calculateCost(edgesFlows[edge.getIndex()] + edgesFlowsAdditions[edge.getIndex()], edge.getCapacity(), initialEdgesCosts[edge.getIndex()]);
                    minimalPathDerivation += costFunction.calculateDerivation(edgesFlows[edge.getIndex()] + edgesFlowsAdditions[edge.getIndex()], edge.getCapacity(), initialEdgesCosts[edge.getIndex()]);
                }
                maximalPathCost = 0.0;
                maximalPathDerivation = 0.0;
                for (TmEdge edge: maximalPath) {
                    maximalPathCost += costFunction.calculateCost(edgesFlows[edge.getIndex()] + edgesFlowsAdditions[edge.getIndex()], edge.getCapacity(), initialEdgesCosts[edge.getIndex()]);
                    maximalPathDerivation += costFunction.calculateDerivation(edgesFlows[edge.getIndex()] + edgesFlowsAdditions[edge.getIndex()], edge.getCapacity(), initialEdgesCosts[edge.getIndex()]);
                }

                if (Double.isNaN(maximalPathDerivation) || Double.isInfinite(maximalPathDerivation)) {
                    throw new ArithmeticException("Newton method cannot handle infinite values.");
                }

                dx += (maximalPathCost - minimalPathCost) / (maximalPathDerivation + minimalPathDerivation);

                if (Double.isNaN(dx) || Double.isInfinite(dx)) {
                    throw new ArithmeticException("Newton method cannot handle infinite values.");
                }

                if (Math.abs(maximalPathCost - minimalPathCost) < 1E-10) {
                    currentIteration = DEFAULT_NEWTON_METHOD_ITERATIONS_COUNT + 10;
                }

                for (TmEdge edge: minimalPath) {
                    edgesFlowsAdditions[edge.getIndex()] = dx;
                }
                for (TmEdge edge: maximalPath) {
                    edgesFlowsAdditions[edge.getIndex()] = -dx;
                }

                currentIteration++;
            }

            if (dx < 0.0) {
                dx = 0.0;
            }
            if (dx > maximalDx) {
                dx = maximalDx;
            }

            for (TmEdge edge: minimalPath) {
                edgesFlowsAdditions[edge.getIndex()] = dx;
            }
            for (TmEdge edge: maximalPath) {
                edgesFlowsAdditions[edge.getIndex()] = -dx;
            }
        }

        //Adding additions to bush and edgesFlows
        for (int i = 0; i < edgesFlowsAdditions.length; i++) {
            bush.incrementEdgeFlow(i, edgesFlowsAdditions[i]);
            edgesFlows[i] += edgesFlowsAdditions[i];
        }
    }

    /**
     * Shifts flows between the minimal flow path and the maximal flow path of the specified bush using the Newton method. The changes in the flows are stored to the bush and to the edges flows additions
     * Utilized for parallel shift of edges flows
     * @param bushIndex the index of the bush, in which the shift of flows shall be performed
     * @param minimalPath minimal path, to which the flows shall be shifted
     * @param maximalPath maximal path, from which the flows shall be shifted
     */
    protected void shiftEdgesFlows(int bushIndex, TmEdge[] minimalPath, TmEdge[] maximalPath) {
        double maximalDx = bushes[bushIndex].getEdgeFlow(maximalPath[0].getIndex());
        for (TmEdge edge: maximalPath) {
            if (bushes[bushIndex].getEdgeFlow(edge.getIndex()) < maximalDx) {
                maximalDx = bushes[bushIndex].getEdgeFlow(edge.getIndex());
            }
        }
        if (maximalDx > 0) {
            double minimalPathCost = 0.0;
            double maximalPathCost = 0.0;
            double minimalPathDerivation = 0.0;
            double maximalPathDerivation = 0.0;
            double dx = 0.0;
            int currentIteration = 0;

            while (currentIteration < DEFAULT_NEWTON_METHOD_ITERATIONS_COUNT) {
                minimalPathCost = 0.0;
                minimalPathDerivation = 0.0;
                for (TmEdge edge: minimalPath) {
                    minimalPathCost += costFunction.calculateCost(edgesFlows[edge.getIndex()] + edgesFlowsAdditions[bushIndex][edge.getIndex()], edge.getCapacity(), initialEdgesCosts[edge.getIndex()]);
                    minimalPathDerivation += costFunction.calculateDerivation(edgesFlows[edge.getIndex()] + edgesFlowsAdditions[bushIndex][edge.getIndex()], edge.getCapacity(), initialEdgesCosts[edge.getIndex()]);
                }
                maximalPathCost = 0.0;
                maximalPathDerivation = 0.0;
                for (TmEdge edge: maximalPath) {
                    maximalPathCost += costFunction.calculateCost(edgesFlows[edge.getIndex()] + edgesFlowsAdditions[bushIndex][edge.getIndex()], edge.getCapacity(), initialEdgesCosts[edge.getIndex()]);
                    maximalPathDerivation += costFunction.calculateDerivation(edgesFlows[edge.getIndex()] + edgesFlowsAdditions[bushIndex][edge.getIndex()], edge.getCapacity(), initialEdgesCosts[edge.getIndex()]);
                }

                if (Double.isNaN(maximalPathDerivation) || Double.isInfinite(maximalPathDerivation)) {
                    throw new ArithmeticException("Newton method cannot handle infinite values.");
                }

                dx += (maximalPathCost - minimalPathCost) / (maximalPathDerivation + minimalPathDerivation);

                if (Double.isNaN(dx) || Double.isInfinite(dx)) {
                    throw new ArithmeticException("Newton method cannot handle infinite values.");
                }

                if (Math.abs(maximalPathCost - minimalPathCost) < 1E-10) {
                    currentIteration = DEFAULT_NEWTON_METHOD_ITERATIONS_COUNT + 10;
                }

                for (TmEdge edge: minimalPath) {
                    edgesFlowsAdditions[bushIndex][edge.getIndex()] = dx;
                }
                for (TmEdge edge: maximalPath) {
                    edgesFlowsAdditions[bushIndex][edge.getIndex()] = -dx;
                }

                currentIteration++;
            }

            if (dx < 0.0) {
                dx = 0.0;
            }
            if (dx > maximalDx) {
                dx = maximalDx;
            }

            for (TmEdge edge: minimalPath) {
                edgesFlowsAdditions[bushIndex][edge.getIndex()] = dx;
            }
            for (TmEdge edge: maximalPath) {
                edgesFlowsAdditions[bushIndex][edge.getIndex()] = -dx;
            }
        }
    }

    /**
     * Performs callback with intermediate results of the computation
     */
    protected void invokeCallback() {
        callbackInvokable.invokeCallback(edgesFlows);
    }

    //Getters and setters

    /**
     * Returns the maximal number of iterations
     * @return the maximal number of iterations
     */
    public int getIterationsCount() {
        return iterationsCount;
    }

    /**
     * Returns the maximal desired epsilon
     * @return the maximal desired epsilon
     */
    public double getEpsilon() {
        return epsilon;
    }

    /**
     * Returns the current iteration
     * @return the current iteration
     */
    public int getCurrentIteration() {
        return currentIteration;
    }

    /**
     * Returns the relative gap
     * @return the relative gap
     */
    public double getRelativeGap() {
        return relativeGap;
    }

    /**
     * Returns the final flows assigned to the edges of the graph once the computation is finished
     * @return the final flows assigned to the edges of the graph once the computation is finished
     */
    public double[] getAssignedEdgesFlows() {
        return assignedEdgesFlows;
    }
}