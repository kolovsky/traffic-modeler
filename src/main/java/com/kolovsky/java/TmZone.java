package com.kolovsky.java;

/**
 * Zone representing either the origin or the destination in the origin-destination matrix
 * Each zone corresponds to a specific node of the graph representing the road traffic network
 * @author Tomas Potuzak
 */
public class TmZone implements Comparable<TmZone> {
    /** The ID of this zone*/
    protected int id;
    /** The node of this zone representing the location of the zone in the graph */
    protected TmNode node;

    /**
     * Creates a new zone with specified ID and node
     * @param id the ID of the zone
     * @param node the node of the zone representing the location of the zone in the graph
     */
    public TmZone(int id, TmNode node) {
        this.id = id;
        this.node = node;
    }

    //Getters and setters

    /**
     * Returns the ID of this zone
     * @return the ID of this zone
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the ID of this zone
     * @param id the new ID of this zone
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Returns the node of this zone
     * @return the node of this zone
     */
    public TmNode getNode() {
        return node;
    }

    /**
     * Sets the node of this zone
     * @param node the new node of this zone
     */
    public void setNode(TmNode node) {
        this.node = node;
    }

    //Inherited methods

    @Override
    public int compareTo(TmZone other) {
        if (id < other.id) {
            return TmComparisonUtils.THIS_LOWER;
        }
        else if (id > other.id) {
            return TmComparisonUtils.THIS_HIGHER;
        }
        else {
            return TmComparisonUtils.EQUAL;
        }
    }

    @Override
    public String toString() {
        return id + " (node ID: " + node.getId() + ")";
    }
}