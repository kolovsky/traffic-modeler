package com.kolovsky.java;

/**
 * A tuple of two values
 * @param <F> first value
 * @param <S> second value
 * @author Tomas Potuzak
 */
public class TmPairElement<F, S> {
    /** First value */
    protected F first;
    /** Second value */
    protected S second;

    /**
     * Creates new pair of values
     * @param first first value
     * @param second second value
     */
    public TmPairElement(F first, S second) {
        this.first = first;
        this.second = second;
    }

    /**
     * Returns the first value
     * @return the first value
     */
    public F getFirst() {
        return first;
    }

    /**
     * Sets the first value
     * @param first the new first value
     */
    public void setFirst(F first) {
        this.first = first;
    }

    /**
     * Returns the second value
     * @return the second value
     */
    public S getSecond() {
        return second;
    }

    /**
     * Sets the second value
     * @param second the new second value
     */
    public void setSecond(S second) {
        this.second = second;
    }

    //Inherited methods

    @Override
    public String toString() {
        return "(" + first + ", " + second + ")";
    }
}