package com.kolovsky.java;

/**
 * An utility class for efficient calculation of powers with integer exponents
 * @author Tomas Potuzak
 */
public class TmMathUtils {

    /**
     * Calculates and returns the power of the specified integer base with specified integer exponent. The power is calculated in a cycle and is more efficient than general <code>Math.pow()</code>
     * @param base the base of the power
     * @param exponent the exponent of the power
     * @return the power of the specified integer base with specified integer exponent
     * @throws IllegalArgumentException if the exponent is negative
     */
    public static int power(int base, int exponent) throws IllegalArgumentException {
        if (exponent < 0) {
            throw new IllegalArgumentException("Exponent cannot be negative.");
        }
        else if (exponent == 0) {
            return 1;
        }
        else {
            int result = 1;

            for (int i = 0; i < exponent; i++) {
                result *= base;
            }

            return result;
        }
    }

    /**
     * Calculates and returns the power of the specified doube base with specified integer exponent. The power is calculated in a cycle and is more efficient than general <code>Math.pow()</code>
     * @param base the base of the power
     * @param exponent the exponent of the power
     * @return the power of the specified integer base with specified integer exponent
     * @throws IllegalArgumentException if the exponent is negative
     */
    public static double power(double base, int exponent) throws IllegalArgumentException {
        if (exponent < 0) {
            throw new IllegalArgumentException("Exponent cannot be negative.");
        }
        else if (exponent == 0) {
            return 1;
        }
        else {
            double result = 1;

            for (int i = 0; i < exponent; i++) {
                result *= base;
            }

            return result;
        }
    }

    /**
     * Calculates and return the second power of the specified integer base
     * @param base the base of the second power
     * @return the second power of the specified integer base
     */
    public static int square(int base) {
        return power(base, 2);
    }

    /**
     * Calculates and return the second power of the specified double base
     * @param base the base of the second power
     * @return the second power of the specified double base
     */
    public static double square(double base) {
        return power(base, 2);
    }
}