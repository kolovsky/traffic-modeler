package com.kolovsky.java;

/**
 * A working thread for the parallel computation of the B static traffic assignment
 * @author Tomas Potuzak
 */
public class TmParallelBWorkingThread extends Thread {
    /** The index of the thread which serves as the control thread of the parallel computation */
    public static final int CONTROL_THREAD_INDEX = 0;
    /** The index of this thread */
    protected int index;
    /** The reference to the instance with B static traffic assignment. The working threads work is performed by invoking methods of this instance */
    protected TmParallelBStaticTrafficAssignment parallelBStaticTrafficAssignment;
    /** The reference to the barrier for the synchronization of the working threads */
    protected TmCyclicBarrier barrier;
    /** The start index (inclusive) of the part of the bushes, which shall be processed by this thread */
    protected int bushesStartIndex;
    /** The end index (exclusive) of the part of the bushes, which shall be processed by this thread */
    protected int bushesEndIndex;
    /** The start index (inclusive) of the part of the edges, which shall be processed by this thread */
    protected int edgesStartIndex;
    /** The end index (inclusive) of the part of the edges, which shall be processed by this thread */
    protected int edgesEndIndex;

    /**
     * Creates a new working thread with specified index, reference to the instance with the B static traffic assignment and the barrier and the indices indicating the parts of the bushes and edges, which shall be processed by this thread
     * @param index the index of the working thread
     * @param parallelBStaticTrafficAssignment the reference to the instance with B static traffic assignment. The working threads work is performed by invoking methods of this instance
     * @param barrier the reference to the barrier for the synchronization of the working threads
     * @param bushesStartIndex the start index (inclusive) of the part of the bushes, which shall be processed by this thread
     * @param bushesEndIndex the end index (exclusive) of the part of the bushes, which shall be processed by this thread
     * @param edgesStartIndex the start index (inclusive) of the part of the edges, which shall be processed by this thread
     * @param edgesEndIndex the end index (inclusive) of the part of the edges, which shall be processed by this thread
     */
    public TmParallelBWorkingThread(int index, TmParallelBStaticTrafficAssignment parallelBStaticTrafficAssignment, TmCyclicBarrier barrier, int bushesStartIndex, int bushesEndIndex, int edgesStartIndex, int edgesEndIndex) {
        this.index = index;
        this.parallelBStaticTrafficAssignment = parallelBStaticTrafficAssignment;
        this.barrier = barrier;
        this.bushesStartIndex = bushesStartIndex;
        this.bushesEndIndex = bushesEndIndex;
        this.edgesStartIndex = edgesStartIndex;
        this.edgesEndIndex = edgesEndIndex;
    }

    @Override
    public void run() {
        parallelBStaticTrafficAssignment.initializeBushes(bushesStartIndex, bushesEndIndex);

        barrier.synchronize(index);

        parallelBStaticTrafficAssignment.calculateEdgesFlows(edgesStartIndex, edgesEndIndex);
        parallelBStaticTrafficAssignment.calculateEdgesCosts(edgesStartIndex, edgesEndIndex);

        barrier.synchronize(index);

        while (parallelBStaticTrafficAssignment.getCurrentIteration() < parallelBStaticTrafficAssignment.getIterationsCount() && parallelBStaticTrafficAssignment.getRelativeGap() > parallelBStaticTrafficAssignment.getEpsilon()) {
            System.out.println("[" + index + "] Iteration " + parallelBStaticTrafficAssignment.getCurrentIteration());

            parallelBStaticTrafficAssignment.improveBushes(bushesStartIndex, bushesEndIndex);
            parallelBStaticTrafficAssignment.findCostTrees(bushesStartIndex, bushesEndIndex);

            barrier.synchronize(index);

            //EXPERIMENT PARALLEL SHIFT EDGES FLOWS
            //parallelBStaticTrafficAssignment.shiftEdgesFlowsParallel(bushesStartIndex, bushesEndIndex);
            //barrier.synchronize(index);


            if (index == CONTROL_THREAD_INDEX) {
                //parallelStmBStaticTrafficAssignment.shiftEdgesFlowsGather();
                parallelBStaticTrafficAssignment.shiftEdgesFlows();
                barrier.synchronize(index);
            }
            else {
                barrier.synchronize(index);
            }

            parallelBStaticTrafficAssignment.removeUnusedEdges(bushesStartIndex, bushesEndIndex);
            parallelBStaticTrafficAssignment.calculateEdgesCosts(edgesStartIndex, edgesEndIndex);

            barrier.synchronize(index);

            parallelBStaticTrafficAssignment.calculateObjectiveFunctionParallel(index, edgesStartIndex, edgesEndIndex);

            barrier.synchronize(index);

            if (index == CONTROL_THREAD_INDEX) {
                parallelBStaticTrafficAssignment.calculateObjectiveFunctionGather();
                barrier.synchronize(index);
            }
            else {
                barrier.synchronize(index);
            }

            parallelBStaticTrafficAssignment.calculateRelativeGapParallel(index, bushesStartIndex, bushesEndIndex);

            barrier.synchronize(index);

            if (index == CONTROL_THREAD_INDEX) {
                parallelBStaticTrafficAssignment.calculateRelativeGapGather();
                parallelBStaticTrafficAssignment.invokeCallback();
                barrier.synchronize(index);
            }
            else {
                barrier.synchronize(index);
            }
        }
    }
}