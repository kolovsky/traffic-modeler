package com.kolovsky.java;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A tree for B assignment
 * @author Tomas Potuzak
 */
public class TmBTree {
    /** Default value of initial distance of nodes */
    public static final int DEFAULT_INITIAL_DISTANCE_VALUE = 0;
    /** The origin serving as root node of the tree */
    protected TmZone origin;
    /** The distances of the nodes to the root node */
    protected double[] distances;
    /** The last edges on the paths from the root node to the individual nodes */
    protected TmEdge[] previousEdges;

    /**
     * Creates a new tree with specified origin, number of nodes, and initial distance value
     * @param origin the origin serving as the root node of the tree
     * @param nodesCount number of nodes in the tree
     * @param initialDistanceValue initial distances of nodes from the root node
     */
    public TmBTree(TmZone origin, int nodesCount, double initialDistanceValue) {
        this.origin = origin;

        distances = new double[nodesCount];
        Arrays.fill(distances, initialDistanceValue);
        previousEdges = new TmEdge[nodesCount];
    }

    /**
     * Creates a new tree with specified origin, number of nodes, and default initial distance value
     * @param origin origin the origin serving as the root node of the tree
     * @param nodesCount number of nodes in the tree
     */
    public TmBTree(TmZone origin, int nodesCount) {
        this(origin, nodesCount, DEFAULT_INITIAL_DISTANCE_VALUE);
    }

    /**
     * Returns the distance of the specified node from the root node
     * @param nodeIndex index of the node, whose distance to the root node shall be returned
     * @return the distance of the specified node from the root node
     */
    public double getDistance(int nodeIndex) {
        return distances[nodeIndex];
    }

    /**
     * Sets the distance of the specified node from the root node
     * @param nodeIndex index of the node, whose distance to the root node shall be set
     * @param distance a new distance of the specified node to the root node
     */
    public void setDistance(int nodeIndex, double distance) {
        distances[nodeIndex] = distance;
    }

    /**
     * Returns the previous edge (i.e., the last edge on the path from the root node) to the specified node
     * @param nodeIndex index of the node, whose previous edge shall be returned
     * @return the previous edge (i.e., the last edge on the path from the root node) to the specified node
     */
    public TmEdge getPreviousEdge(int nodeIndex) {
        return previousEdges[nodeIndex];
    }

    /**
     * Sets the previous edge (i.e., the last edge on the path from the root node) to the specified node
     * @param nodeIndex index of the node, whose previous edge shall be set
     * @param previousEdge a new previous edge to the specified node
     */
    public void setPreviousEdge(int nodeIndex, TmEdge previousEdge) {
        previousEdges[nodeIndex] = previousEdge;
    }

    /**
     * Finds and returns the path from the specified node to the root node
     * @param nodeIndex index of the node, from which the path to the root node shall be found
     * @return the path from the specified node to the root node
     */
    public TmEdge[] findPathToOrigin(int nodeIndex) {
        List<TmEdge> pathEdges = new ArrayList<>();

        TmEdge pathEdge = previousEdges[nodeIndex];
        while (pathEdge != null) {
            pathEdges.add(pathEdge);
            pathEdge = previousEdges[pathEdge.getStartNode().getIndex()];
        }

        TmEdge[] path = new TmEdge[pathEdges.size()];
        for (int i = 0; i < path.length; i++) {
            path[i] = pathEdges.get(path.length - i - 1);
        }
        return path;
    }

    //Getters and setters

    /**
     * Returns the origin of this tree
     * @return the origin of this tree
     */
    public TmZone getOrigin() {
        return origin;
    }

    /**
     * Returns the distances of the nodes to the root node
     * @return the distances of the nodes to the root node
     */
    public double[] getDistances() {
        return distances;
    }

    /**
     * Returns the last edges on the paths from the root node to the individual nodes
     * @return the last edges on the paths from the root node to the individual nodes
     */
    public TmEdge[] getPreviousEdges() {
        return previousEdges;
    }
}