package com.kolovsky.java;

/**
 * An interface for invocation of a callback method for data delivery
 * @author Tomas Potuzak
 */
public interface TmCallbackInvokable {

    /**
     * Performs a callback invocation with specified data
     * @param data data to be delivered
     */
    public void invokeCallback(double[] data);
}