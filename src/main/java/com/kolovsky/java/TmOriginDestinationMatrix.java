package com.kolovsky.java;

import java.util.ArrayList;
import java.util.List;

/**
 * The origin-destination matrix representing the flows of vehicles from origin nodes to destination nodes for the road traffic network
 * @author Tomas Potuzak
 */
public class TmOriginDestinationMatrix {
    /** Index in the fast row access table, on which the start indices are */
    protected int ACCESS_TABLE_START_INDEX = 0;
    /** Index in the fast row access table, on which the end indices are */
    protected int ACCESS_TABLE_END_INDEX = 1;
    /** Index in the fast row access table, on which the row lengths are */
    protected int ACCESS_TABLE_ROW_LENGTH = 2;
    /** Pairs representing flows from an origin node a destination node (i.e., row of the origin destination matrix) */
    protected List<TmOriginDestinationPair> originDestinationPairs;
    /** Minimal origin ID used for fast row access table */
    protected int minimalOriginId;
    /** Maximal origin ID used for fast row access table */
    protected int maximalOriginId;
    /** Fast row access table utilized for direct access to origin-destination pairs (i.e., rows of the matrix) based on origin IDs */
    protected int[][] originRowsAccessTable;
    /** All the origins of the origin-destination matrix */
    protected TmZone[] origins;

    /**
     * Creates a new empty origin-destination matrix
     */
    public TmOriginDestinationMatrix() {
        originDestinationPairs = new ArrayList<>();

        minimalOriginId = Integer.MAX_VALUE;
        maximalOriginId = Integer.MIN_VALUE;
    }

    /**
     * Initializes the fast row access table. This method shall be invoked <b>after</b> all origin-destination pairs have been added to this matrix. Otherwise, <code>NullPointerException is likely to occur during the access to the origin-destination pairs (i.e., rows)</code>
     */
    public void inititializeOriginRowAccessTable() {
        originDestinationPairs.sort(null); //To ensure the pairs are sorter in ascending manner by the origin ID

        originRowsAccessTable = new int[maximalOriginId - minimalOriginId + 1][ACCESS_TABLE_ROW_LENGTH + 1];
        List<TmZone> originsList = new ArrayList<>();

        int lastOriginId = Integer.MIN_VALUE;
        int originId = 0;
        for (int i = 0; i < originDestinationPairs.size(); i++) {
            originId = originDestinationPairs.get(i).getOrigin().getId();
            if (originId != lastOriginId) {
                //System.out.println("Origin ID: " + originId);
                originsList.add(originDestinationPairs.get(i).getOrigin());
                if (lastOriginId >= 0) {
                    originRowsAccessTable[lastOriginId - minimalOriginId][ACCESS_TABLE_END_INDEX] = i;
                    originRowsAccessTable[lastOriginId - minimalOriginId][ACCESS_TABLE_ROW_LENGTH] = originRowsAccessTable[lastOriginId - minimalOriginId][ACCESS_TABLE_END_INDEX] - originRowsAccessTable[lastOriginId - minimalOriginId][ACCESS_TABLE_START_INDEX];
                }
                originRowsAccessTable[originId - minimalOriginId][ACCESS_TABLE_START_INDEX] = i;
                lastOriginId = originId;
            }
        }
        originRowsAccessTable[lastOriginId - minimalOriginId][ACCESS_TABLE_END_INDEX] = originDestinationPairs.size();
        originRowsAccessTable[lastOriginId - minimalOriginId][ACCESS_TABLE_ROW_LENGTH] = originRowsAccessTable[lastOriginId - minimalOriginId][ACCESS_TABLE_END_INDEX] - originRowsAccessTable[lastOriginId - minimalOriginId][ACCESS_TABLE_START_INDEX];

        origins = originsList.toArray(new TmZone[originsList.size()]);
    }

    /**
     * Returns the origin-destination pair (i.e., row) based on the specified origin ID and destination index
     * @param originId the ID of the origin of the searched origin-destination pair
     * @param destinationIndex the index of the destination of the searched origin-destination pair
     * @return
     */
    public TmOriginDestinationPair getOriginDestinationPair(int originId, int destinationIndex) {
        return originDestinationPairs.get(originRowsAccessTable[originId - minimalOriginId][ACCESS_TABLE_START_INDEX] + destinationIndex);
    }

    /**
     * Returns the number of destinations for the specified origin ID
     * @param originId the ID of the of the origin, for which the number of destinations shall be returned
     * @return the number of destinations for the specified origin ID
     */
    public int getOriginDestinationsCount(int originId) {
        return originRowsAccessTable[originId - minimalOriginId][ACCESS_TABLE_ROW_LENGTH];
    }

    /**
     * Adds the specified origin-destination pair (i.e., row) to this origin-destination matrix
     * @param originDestinationPair the origin-destination pair, which shall be added to this matrix
     */
    public void addOriginDestinationPair(TmOriginDestinationPair originDestinationPair) {
        originDestinationPairs.add(originDestinationPair);

        if (originDestinationPair.getOrigin().getId() < minimalOriginId) {
            minimalOriginId = originDestinationPair.getOrigin().getId();
        }
        if (originDestinationPair.getOrigin().getId() > maximalOriginId) {
            maximalOriginId = originDestinationPair.getOrigin().getId();
        }
    }

    //Getters and setters

    /**
     * Returns all the origin-destination pairs (i.e., rows) of this matrix
     * @return all the origin-destination pairs (i.e., rows) of this matrix
     */
    public List<TmOriginDestinationPair> getOriginDestinationPairs() {
        return originDestinationPairs;
    }

    /**
     * Returns the origins of this matrix
     * @return the origins of this matrix
     */
    public TmZone[] getOrigins() {
        return origins;
    }
}