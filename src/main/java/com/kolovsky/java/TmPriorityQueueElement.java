package com.kolovsky.java;

/**
 * Element of the priority queue
 * @param <P> the priority of the element
 * @param <V> the value of the element
 * @author Tomas Potuzak
 */
public class TmPriorityQueueElement<P extends Comparable<P>, V> implements Comparable<TmPriorityQueueElement<P, V>> {
    /** Priority of the element */
    protected P priority;
    /** Value of the element */
    protected V value;

    /**
     * Creates a new priority queue element with specified priority and value
     * @param priority priority of the new element
     * @param value value of the new element
     * @throws NullPointerException when the priority is <code>null</code>
     */
    public TmPriorityQueueElement(P priority, V value) throws NullPointerException {
        if (priority == null) {
            throw new NullPointerException("Priority cannot be null.");
        }

        this.priority = priority;
        this.value = value;
    }

    //Getters and setters

    /**
     * Returns the priority of the element
     * @return the priority of the element
     */
    public P getPriority() {
        return priority;
    }

    /**
     * Sets the priority of the element
     * @param priority the new priority of the element
     */
    public void setPriority(P priority) {
        this.priority = priority;
    }

    /**
     * Returns the value of the element
     * @return the value of the element
     */
    public V getValue() {
        return value;
    }

    /**
     * Sets the value of the element
     * @param value the new value of he element
     */
    public void setValue(V value) {
        this.value = value;
    }

    //Inherited methods

    @Override
    public int compareTo(TmPriorityQueueElement<P, V> other) {
        return priority.compareTo(other.priority);
    }

    @Override
    public String toString() {
        return priority + ": " + value;
    }
}