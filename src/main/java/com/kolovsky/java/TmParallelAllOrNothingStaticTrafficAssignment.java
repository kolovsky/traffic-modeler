package com.kolovsky.java;

import java.util.Arrays;

/**
 * A class with parallel all-or-nothing static traffic assignment. The instance of this class have methods, which shall be invoked by working threads of the parallel computation
 * @author Tomas Potuzak
 */
public class TmParallelAllOrNothingStaticTrafficAssignment {
    /** The graph representing the road traffic network */
    protected TmGraph graph;
    /** The origin-destination matrix representing the flows of vehicles from origin nodes to destination nodes for the road traffic network */
    protected TmOriginDestinationMatrix originDestinationMatrix;
    /** Initial costs of the edges of the graph */
    protected double[] initialEdgesCosts;
    /** Parts of the flows assigned to edges during the computation performed by the working threads - necessary to ensure that the parallel computation result is identical with the sequential computation */
    protected double[][] edgesFlows;
    /** Final flows assigned to the edges once the computation is finished */
    protected double[] assignedEdgesFlows;

    /**
     * Creates new instance for the parallel all-or-nothing static traffic assignment
     * @param graph the graph representing the road traffic network
     * @param originDestinationMatrix the origin-destination matrix representing the flows of vehicles from origin nodes to destination nodes for the road traffic network
     * @param initialEdgesCosts initial costs of the edges of the graph
     * @param workingThreadsCount the number of working threads, which shall be created to perform the parallel computation
     */
    public TmParallelAllOrNothingStaticTrafficAssignment(TmGraph graph, TmOriginDestinationMatrix originDestinationMatrix, double[] initialEdgesCosts, int workingThreadsCount) {
        this.graph = graph;
        this.originDestinationMatrix = originDestinationMatrix;
        this.initialEdgesCosts = initialEdgesCosts;

        edgesFlows = new double[workingThreadsCount][initialEdgesCosts.length];
        assignedEdgesFlows = new double[initialEdgesCosts.length];
    }

    /**
     * Adds flow to the edges of the graph based on the origin-destination matrix for the origin in range from start index (inclusive) to end index (exclusive).
     * This method shall be performed by a working thread with specified index
     * @param threadIndex the index of the working thread, which is invoking this method
     * @param originsStartIndex the start index (inclusive) of the origin, from which the flows shall be added to the edges of the graph
     * @param originsEndIndex the end index (exclusive) of the origin, from which the flows shall be added to the edges of the graph
     */
    public void addFlowsFromOrigins(int threadIndex, int originsStartIndex, int originsEndIndex) {
        for (int i = originsStartIndex; i < originsEndIndex; i++) {
            addFlowsFromOrigin(threadIndex, originDestinationMatrix.getOrigins()[i]);
        }
    }

    /**
     * Sums the flows assigned to the edges by the individual working threads to the final flows assigned to the edges of the graph
     */
    public void gatherEdgesFlows() {
        Arrays.fill(assignedEdgesFlows, 0.0);

        for (int i = 0; i < edgesFlows.length; i++) {
            for (int j = 0; j < edgesFlows[i].length; j++) {
                assignedEdgesFlows[j] += edgesFlows[i][j];
                edgesFlows[i][j] = 0.0;
            }
        }
    }

    /**
     * Add flows from the specified origin to the edges of the graph
     * @param origin the origin, from which the flows shall be added to the edges of the graph
     */
    public void addFlowsFromOrigin(int threadIndex, TmZone origin) {
        TmEdge[] nodesPreviousEdges = graph.determineNodesPreviousEdges(origin, initialEdgesCosts);
        TmEdge[] path = null;
        TmOriginDestinationPair pair = null;

        for (int i = 0; i < originDestinationMatrix.getOriginDestinationsCount(origin.getId()); i++) {
            pair = originDestinationMatrix.getOriginDestinationPair(origin.getId(), i);
            path = graph.extractPath(pair.getDestination(), nodesPreviousEdges);

            for (TmEdge edge: path) {
                edgesFlows[threadIndex][edge.getIndex()] += pair.getFlow();
            }
        }
    }

    //Getters and setters

    /**
     * Returns the initial costs of all edges of the graph
     * @return the initial costs of all edges of the graph
     */
    public double[] getInitialEdgesCosts() {
        return initialEdgesCosts;
    }

    /**
     * Sets the initial costs of all edges of the graph
     * @param initialEdgesCosts the new initial costs of all edges of the graph
     */
    public void setInitialEdgesCosts(double[] initialEdgesCosts) {
        this.initialEdgesCosts = initialEdgesCosts;
    }

    /**
     * Returns the final flows assigned to the edges once the computation is finished
     * @return the final flows assigned to the edges once the computation is finished
     */
    public double[] getAssignedEdgesFlows() {
        return assignedEdgesFlows;
    }
}