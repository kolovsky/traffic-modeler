package com.kolovsky.java;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;

/**
 * Connection to the database with methods for reading of graph and origin-destination matrix
 * @author Tomas Potuzak
 */
public class TmDbConnector {
    /** Driver of the database */
    public static final String DATABASE_DRIVER = "org.postgresql.Driver";
    /** Name of the table with edges */
    public static final String TABLE_EDGES = "edge";
    /** Column of the table with edges with IDs of edges*/
    public static final String COLUMN_EDGES_ID = "edge_id";
    /** Column of the table with edges with source node IDs */
    public static final String COLUMN_EDGES_SOURCE = "source";
    /** Column of the table with edges with target node IDs */
    public static final String COLUMN_EDGES_TARGET = "target";
    /** Column of the table with edges with capacity of the edges */
    public static final String COLUMN_EDGES_CAPACITY = "capacity";
    /** Column of the table with edges with costs of the edges */
    public static final String COLUMN_EDGES_COST = "cost";
    /** Column of the table with edges with geometry of the edges */
    public static final String COLUMN_EDGES_GEOMETRY = "geometry";
    /** Column of the table with edges inicating whether the edge is valid */
    public static final String COLUMN_EDGES_VALID = "isvalid";
    /** Name of the table with origin-destination matrix */
    public static final String TABLE_ODM = "odm";
    /** Column of the table with origin-destination matrix with source zone IDs*/
    public static final String COLUMN_ODM_SOURCE_ZONE_ID = "source";
    /** Column of the table with origin-destination matrix with source node IDs */
    public static final String COLUMN_ODM_SOURCE_NODE_ID = "source_node";
    /** Column of the table with origin-destination matrix with target zone IDs */
    public static final String COLUMN_ODM_TARGET_ZONE_ID = "target";
    /** Column of the table with origin-destination matrix with target node IDs */
    public static final String COLUMN_ODM_TARGET_NODE_ID = "target_node";
    /** Column of the table with origin-destination matrix with flows */
    public static final String COLUMN_ODM_FLOW = "flow";
    /** Expected maximal number of nodes - the actual number can be higher, but for lower numbers, no array expansion is performed */
    public static final int EXPECTED_MAXIMAL_NODE_ID = 100_000;

    /** The URL of the database */
    protected String dbUrl;
    /** The JDBC connection to the database */
    protected Connection connection;
    /** The statement for performing of SQL queries */
    protected Statement statement;
    /** All nodes extracted from the table with edges */
    protected TmNode[] nodes;

    /**
     * Creates a new connection to the database based on specified database URL
     * @param dbUrl database URL
     */
    public TmDbConnector(String dbUrl) {
        this.dbUrl = dbUrl;
    }

    /**
     * Connects to the database using JDBC driver
     * @throws ClassNotFoundException if the database driver is not found
     * @throws SQLException if there was a problem with establishing the connection (e.g., invalid user of password)
     */
    public void createConnection() throws ClassNotFoundException, SQLException {
        System.out.println("Connecting to database...");
        Class.forName(DATABASE_DRIVER);
        connection = DriverManager.getConnection(dbUrl);
        statement = connection.createStatement();
        System.out.println("Connection to successfully database established.");
    }

    /**
     * Disconnects from the database
     * @throws SQLException if there was a problem with closing the connection to the database
     */
    public void closeConnection() throws SQLException {
        if (connection == null) {
            throw new SQLException("Connection already closed.");
        }
        connection.close();
        connection = null;
    }

    /**
     * Performs an SQL query (i.e. a SELECT) and returns the result of the query
     * @param sqlQuery a SELECT SQL query
     * @return the result of the query
     * @throws SQLException if there was a problem with the query
     */
    public ResultSet performSQLQuery(String sqlQuery) throws SQLException {
        return statement.executeQuery(sqlQuery);
    }

    /**
     * Performs an SQL statement (e.g., INSERT, UPDATE, etc.)
     * @param sqlStatement a SQL statement
     * @throws SQLException if there was a problem with the query
     */
    public void performSQLStatement(String sqlStatement) throws SQLException {
        statement.executeUpdate(sqlStatement);
    }

    /**
     * Reads and returns the graph from the database based on the specified model name
     * @param modelName the name of model, whose graph shall be read from the database
     * @return the graph from the database based on the specified model name
     * @throws SQLException if there was a problem during the reading the graph
     */
    public TmGraph readGraph(String modelName) throws SQLException {
        ResultSet resultSet = performSQLQuery("SELECT " + COLUMN_EDGES_ID + ", " + COLUMN_EDGES_SOURCE + ", " + COLUMN_EDGES_TARGET + ", " + COLUMN_EDGES_COST + ", " + COLUMN_EDGES_CAPACITY + ", " + COLUMN_EDGES_VALID + " FROM " + modelName + "." + TABLE_EDGES + " ORDER BY " + COLUMN_EDGES_ID);
        if (resultSet != null) {
            nodes = new TmNode[EXPECTED_MAXIMAL_NODE_ID];
            TmGraph graph = new TmGraph(modelName);
            TmEdge edge = null;
            int roadId = 0;
            int startCrossroadId = 0;
            int endCrossroadId = 0;
            TmNode startCrossroad = null;
            TmNode endCrossroad = null;
            double roadCapacity = 0.0;
            double roadCost = 0.0;
            boolean roadValid = false;

            while (resultSet.next()) {
                roadId = resultSet.getInt(1);
                startCrossroadId = resultSet.getInt(2);
                endCrossroadId = resultSet.getInt(3);
                roadCost = resultSet.getDouble(4);
                roadCapacity = resultSet.getDouble(5);
                roadValid = resultSet.getBoolean(6);

                edge = new TmEdge(roadId, roadCapacity, roadCost, roadValid);
                startCrossroad = getNodeById(startCrossroadId);
                endCrossroad = getNodeById(endCrossroadId);
                edge.setStartNode(startCrossroad);
                edge.setEndNode(endCrossroad);
                startCrossroad.addOutgoingEdge(edge);
                endCrossroad.addIncomingEdge(edge);
                graph.addEdge(edge);
            }

            for (TmNode node: nodes) {
                if (node != null) {
                    graph.addNode(node);
                }
            }
            graph.initializeEdgesAccessTable();
            graph.initializeNodesAccessTable();

            return graph;
        }
        else {
            return null;
        }
    }

    /**
     * Returns the node based on the specified ID
     * @param nodeId the ID of the node, which shall be returned
     * @return the node based on the specified ID
     */
    public TmNode getNodeById(int nodeId) {
        if (nodeId >= nodes.length) {
            int newLength = (nodes.length * 2 > nodeId + 1) ? nodes.length * 2 : nodeId + 1;
            nodes = Arrays.copyOf(nodes, newLength);
        }

        if (nodes[nodeId] == null) {
            nodes[nodeId] = new TmNode(nodeId);
        }

        return nodes[nodeId];
    }

    /**
     * Reads and returns the origin-destination matrix from the database based on the specified model name and using the specified graph
     * @param modelName the name of model, whose origin-destination matrix shall be read
     * @param graph the graph of the model. The models must be the same.
     * @return the origin-destination matrix from the database based on the specified model name and using the specified graph
     * @throws SQLException if there was a problem during the reading the origin-destination matrix
     */
    public TmOriginDestinationMatrix readOriginDestinationMatrix(String modelName, TmGraph graph) throws SQLException {
        ResultSet resultSet = performSQLQuery("SELECT " + COLUMN_ODM_SOURCE_ZONE_ID + ", " + COLUMN_ODM_SOURCE_NODE_ID + ", " + COLUMN_ODM_TARGET_ZONE_ID + ", " + COLUMN_ODM_TARGET_NODE_ID + ", " + COLUMN_ODM_FLOW + " FROM " + modelName + "." + TABLE_ODM);
        if (resultSet != null) {
            TmOriginDestinationMatrix originDestinationMatrix = new TmOriginDestinationMatrix();
            TmOriginDestinationPair originDestinationPair = null;
            TmZone origin = null;
            TmZone destination = null;
            TmNode originNode = null;
            TmNode destinationNode = null;
            int originId = 0;
            int destinationId = 0;
            int originNodeId = 0;
            int destinationNodeId = 0;
            double flow = 0.0;

            while (resultSet.next()) {
                originId = resultSet.getInt(1);
                originNodeId = resultSet.getInt(2);
                destinationId = resultSet.getInt(3);
                destinationNodeId = resultSet.getInt(4);
                flow = resultSet.getDouble(5);
                originNode = graph.getNodeById(originNodeId);
                destinationNode = graph.getNodeById(destinationNodeId);
                origin = new TmZone(originId, originNode);
                destination = new TmZone(destinationId, destinationNode);
                originDestinationPair = new TmOriginDestinationPair(origin, destination, flow);
                originDestinationMatrix.addOriginDestinationPair(originDestinationPair);
                //System.out.println(originDestinationPair);
            }
            originDestinationMatrix.inititializeOriginRowAccessTable();

            return originDestinationMatrix;
        }
        else {
            return null;
        }
    }
}