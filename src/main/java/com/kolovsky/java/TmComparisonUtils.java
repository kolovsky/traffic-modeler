package com.kolovsky.java;

/**
 * Utility class for comparison of objects
 * @author Tomas Potuzak
 */
public class TmComparisonUtils {
    /** Constant for the compare() and compareTo() methods indicating that the first instance is lower than the other */
    public static final int THIS_LOWER = -1;
    /** Constant for the compare() and compareTo() methods indicating that the first instance is higher than the other */
    public static final int THIS_HIGHER = 1;
    /** Constant for the compare() and compareTo() methods indicating that the instances are equal */
    public static final int EQUAL = 0;
}