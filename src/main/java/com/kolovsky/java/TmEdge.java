package com.kolovsky.java;

/**
 * An edge of the graph representing the road traffic network
 * @author Tomas Potuzak
 */
public class TmEdge implements Comparable<TmEdge> {
    /** The ID of the graph edge */
    protected int id;
    /** The index of the graph edge in the list of all edges */
    protected int index;
    /** The graph node where this graph edge starts */
    protected TmNode startNode;
    /** The graph node where this graph edge ends */
    protected TmNode endNode;
    /** The capacity of this graph edge */
    protected double capacity;
    /** The cost of this graph edge */
    protected double cost;
    /** Determines whether this graph edge is valid (i.e. is considered to exist and is used for computations) */
    protected boolean valid;
    /** The flow of the traffic road (graph edge) */
    protected double flow;

    /**
     * Creates a new graph edge with specified ID, capacity, cost, and validity
     * @param id the ID of the graph edge
     * @param capacity the capacity of the graph edge
     * @param cost the cost of the graph edge
     * @param valid indicates whether the edge is valid
     */
    public TmEdge(int id, double capacity, double cost, boolean valid) {
        this.id = id;
        this.capacity = capacity;
        this.cost = cost;
        this.valid = valid;
    }

    //Getters and setters

    /**
     * Returns the ID of this graph edge
     * @return the ID of this graph edge
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the ID of this graph edge
     * @param id the new ID of this graph edge
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Returns the index of this graph edge
     * @return the index of this graph edge
     */
    public int getIndex() {
        return index;
    }

    /**
     * Sets the index of this graph edge
     * @param index the new index of this graph edge
     */
    public void setIndex(int index) {
        this.index = index;
    }

    /**
     * Returns the start node of this graph edge
     * @return the start node of this graph edge
     */
    public TmNode getStartNode() {
        return startNode;
    }

    /**
     * Sets the start node of this graph edge
     * @param startNode the new start node of this graph edge
     */
    public void setStartNode(TmNode startNode) {
        this.startNode = startNode;
    }

    /**
     * Returns the end node of this graph edge
     * @return the end node of this graph edge
     */
    public TmNode getEndNode() {
        return endNode;
    }

    /**
     * Sets the end node of this graph edge
     * @param endNode the new end node of this graph edge
     */
    public void setEndNode(TmNode endNode) {
        this.endNode = endNode;
    }

    /**
     * Returns the capacity of this graph edge
     * @return the capacity of this graph edge
     */
    public double getCapacity() {
        return capacity;
    }

    /**
     * Sets the capacity of this graph edge
     * @param capacity the new capacity of this graph edge
     */
    public void setCapacity(double capacity) {
        this.capacity = capacity;
    }

    /**
     * Returns the cost of this graph edge
     * @return the cost of this graph edge
     */
    public double getCost() {
        return cost;
    }

    /**
     * Sets the cost of this graph edge
     * @param cost the new cost of this graph edge
     */
    public void setCost(double cost) {
        this.cost = cost;
    }

    /**
     * Returns <code>true</code> if this edge is valid. Returns <code>false</code> otherwise
     * @return <code>true</code> if this edge is valid. Returns <code>false</code> otherwise
     */
    public boolean isValid() {
        return valid;
    }

    /**
     * Sets whether this edge is valid
     * @param valid value indicating whether this edge shall be valid or not
     */
    public void setValid(boolean valid) {
        this.valid = valid;
    }

    /**
     * Returns the flow of this graph edge
     * @return the flow of this graph edge
     */
    public double getFlow() {
        return flow;
    }

    /**
     * Sets the flow of this graph edge
     * @param flow the new flow of this graph edge
     */
    public void setFlow(double flow) {
        this.flow = flow;
    }

    //Inherited methods

    @Override
    public int compareTo(TmEdge other) {
        if (id < other.id) {
            return TmComparisonUtils.THIS_LOWER;
        }
        else if (id > other.id) {
            return TmComparisonUtils.THIS_HIGHER;
        }
        else {
            return TmComparisonUtils.EQUAL;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof TmEdge) {
            TmEdge other = (TmEdge) o;

            if (id == other.id) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "Edge ID: " + id + ", index: " + index;
    }
}