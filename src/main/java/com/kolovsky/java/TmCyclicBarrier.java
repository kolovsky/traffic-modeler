package com.kolovsky.java;

import java.util.Arrays;

/**
 * Spurious-wakeup-safe cyclic barrier for the synchronization of multiple working threads.
 * Threads must use unique indices in range from zero to number of threads minus one
 * @author Tomas Potuzak
 */
public class TmCyclicBarrier {
    /** Total number of threads, which shall be synchronized on this barrier */
    protected int totalThreadsCount;
    /** The number of currently waiting threads on this barrier */
    protected int waitingThreadsCount;
    /** Flags indicating that the thread with the corresponding index must wait on this barrier (i.e., it there is a spurious wakeup, the thread falls asleep again) */
    protected boolean[] mustThreadsWait;

    /**
     * Creates new barrier with specified number of threads
     * @param totalThreadsCount the number of threads, which shall be synchronized on this barrier
     */
    public TmCyclicBarrier(int totalThreadsCount) {
        this.totalThreadsCount = totalThreadsCount;
        mustThreadsWait = new boolean[totalThreadsCount];
        Arrays.fill(mustThreadsWait, true);
    }

    /**
     * Method which shall be invoked by the thread, which wants to be synchronized on this barrier
     * @param threadIndex the index of the invoking thread
     */
    public synchronized void synchronize(int threadIndex) {
        waitingThreadsCount++;

        if (waitingThreadsCount == totalThreadsCount) {
            waitingThreadsCount = 0;
            Arrays.fill(mustThreadsWait, false);
            notifyAll();
        }
        else {
            mustThreadsWait[threadIndex] = true;
            while (mustThreadsWait[threadIndex]) {
                try {
                    wait();
                }
                catch (InterruptedException ex) {
                    //No action - fall asleep again when woke up unintentionally
                }
            }
        }
    }
}