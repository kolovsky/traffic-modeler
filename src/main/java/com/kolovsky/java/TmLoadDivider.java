package com.kolovsky.java;

import java.util.Arrays;

/**
 * Class for division of load among threads and/or processes
 * @author Tomas Potuzak
 */
public class TmLoadDivider {

    /**
     * Calculates division of an array of specified length into specified number of similar (same-sized if possible) parts. The results is a two-dimensional two-line integer array representing the start (zeroth line) and end (first line) indices
     * @param targetsCount the number of parts
     * @param arrayLength the length of the divided array
     * @return two-dimensional two-line integer array representing the start (zeroth line) and end (first line) indices
     */
    public static int[][] divideLoad(int targetsCount, int arrayLength) {
        double arrayLengthPerTarget = (double) arrayLength / targetsCount;
        int integerArrayLengthPerTarget = arrayLength / targetsCount;
        int[][] indices = new int[2][targetsCount];
        double remainder = 0;
        int shift = 0;

        for (int i = 0; i < targetsCount; i++) {
            remainder += arrayLengthPerTarget - integerArrayLengthPerTarget;

            if (remainder >= 1.0) {
                indices[0][i] = i * integerArrayLengthPerTarget + shift;
                indices[1][i] = (i + 1) * integerArrayLengthPerTarget + shift + 1;
                remainder -= 1.0;
                shift++;
            }
            else {
                indices[0][i] = i * integerArrayLengthPerTarget + shift;
                indices[1][i] = (i + 1) * integerArrayLengthPerTarget + shift;
            }

            if (indices[0][i] > arrayLength - 1 && arrayLength - 1 >= 0) {
                indices[0][i] = arrayLength - 1;
            }
            if (indices[1][i] > arrayLength && arrayLength >= 0) {
                indices[1][i] = arrayLength;
            }
        }

        if (indices[1][targetsCount - 1] < arrayLength) {
            indices[1][targetsCount - 1] = arrayLength;
        }

        System.out.println("Load divided: ");
        System.out.println(Arrays.toString(indices[0]));
        System.out.println(Arrays.toString(indices[1]) + "\n");

        return indices;
    }
}