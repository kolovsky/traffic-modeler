package com.kolovsky.java;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * The graph representing the road traffic network
 * @author Tomas Potuzak
 */
public class TmGraph {
    /** Index of minimal cost tree in return value containing both minimal and maximal cost trees */
    public static final int MINIMAL_COST_TREE = 0;
    /** Index of maximal cost tree in return value containing both minimal and maximal cost trees */
    public static final int MAXIMAL_COST_TREE = 1;
    /** The name of model, which is represented by this graph */
    protected String modelName;
    /** The edges of this graph */
    protected List<TmEdge> edges;
    /** The nodes of this graph */
    protected List<TmNode> nodes;
    /** Minimal ID of all edges utilized for fast edges access table */
    protected int minimalEdgeId;
    /** Maximal ID of all edges utilized for fast edges access table */
    protected int maximalEdgeId;
    /** Minimal ID of all nodes utilized for fast nodes access table */
    protected int minimalNodeId;
    /** Maximal ID of all nodes utilized for fast nodes access table */
    protected int maximalNodeId;
    /** Fast edges access table utilized for direct access to edges based on their indices */
    protected TmEdge[] edgesAccessTable;
    /** Fast nodes access table utilized for direct access to nodes based on their indices */
    protected TmNode[] nodesAccessTable;

    /**
     * Creates a new graph with specified model name
     * @param modelName the name of model, which shall be represented by the graph
     */
    public TmGraph(String modelName) {
        this.modelName = modelName;

        edges = new ArrayList<>();
        nodes = new ArrayList<>();
        minimalEdgeId = Integer.MAX_VALUE;
        maximalEdgeId = Integer.MIN_VALUE;
        minimalNodeId = Integer.MAX_VALUE;
        maximalNodeId = Integer.MIN_VALUE;
    }

    /**
     * Initializes the fast edges access table. This method shall be invoked <b>after</b> all edges have been added to this graph. Otherwise, <code>ArrayIndexOutOfBoundsException is likely to occur during the access to edges</code>
     */
    public void initializeEdgesAccessTable() {
        edgesAccessTable = new TmEdge[maximalEdgeId - minimalEdgeId + 1];
        TmEdge edge = null;
        for (int i = 0; i < edges.size(); i++) {
            edge = edges.get(i);
            edge.setIndex(i);
            edgesAccessTable[edge.getId() - minimalEdgeId] = edge;
        }
    }

    /**
     * Initializes the fast edges access table. This method shall be invoked <b>after</b> all nodes have been added to this graph. Otherwise, <code>ArrayIndexOutOfBoundsException is likely to occur during the access to nodes</code>
     */
    public void initializeNodesAccessTable() {
        nodesAccessTable = new TmNode[maximalNodeId - minimalNodeId + 1];
        TmNode node = null;
        for (int i = 0; i < nodes.size(); i++) {
            node = nodes.get(i);
            node.setIndex(i);
            nodesAccessTable[node.getId() - minimalNodeId] = node;
        }
    }

    /**
     * Returns the edge based on the specified ID using the fast edges access table.
     * The <code>initializeEdgesAccessTable()</code> method must be invoked prior invocation of this method
     * @param edgeId the ID of the edge
     * @return the edge based on the specified ID using the fast edges access table
     */
    public TmEdge getEdgeById(int edgeId) {
        return edgesAccessTable[edgeId - minimalEdgeId];
    }

    /**
     * Returns the node based on the specified ID using the fast nodes access table.
     * The <code>initializeNodesAccessTable()</code> method must be invoked prior invocation of this method
     * @param nodeId the ID of the node
     * @return the node based on the specified ID using the fast nodes access table
     */
    public TmNode getNodeById(int nodeId) {
        return nodesAccessTable[nodeId - minimalNodeId];
    }

    /**
     * Returns the bush created from this graph using the specified origin and costs of all edges of the graph
     * @param originNodeId the origin serving as root node of the bush
     * @param edgesCosts the cost of all edges of the graph
     * @return the bush created from this graph using the specified origin and costs of all edges of the graph
     * @throws IllegalArgumentException if the number of edges does not correspond to the number of costs of all edges
     */
    public TmBBush getBush(TmZone origin, double[] edgesCosts) throws IllegalArgumentException {
        if (edgesCosts.length != edges.size()) {
            throw new IllegalArgumentException("The edges costs count must correspond to the edges count.");
        }

        TmBBush bush = new TmBBush(origin, edgesCosts.length);
        double[] nodesDistances = determineNodesDistances(origin, edgesCosts);

        TmEdge edge = null;
        for (int i = 0; i < edges.size(); i++) {
            edge = edges.get(i);

            if (nodesDistances[edge.getEndNode().getIndex()] > nodesDistances[edge.getStartNode().getIndex()]) {
                bush.setEdgeMask(i, true);
            }
            else {
                bush.setEdgeMask(i, false);
            }
        }

        return bush;
    }

    /**
     * Determines and returns the distances of all nodes to the origin serving as the root node of a bush
     * @param origin the origin serving as the root node of a bush
     * @param edgesCosts costs of all edges of the graph
     * @return the distances of all nodes to the origin serving as the root node of a bush
     * @throws IllegalArgumentException if the number of edges does not correspond to the number of costs of all edges
     */
    public double[] determineNodesDistances(TmZone origin, double[] edgesCosts) throws IllegalArgumentException {
        return determineNodesDistancesDijkstra(origin, edgesCosts);
    }

    /**
     * Determines and returns the distances of all nodes to the origin serving as the root node of a bush using the Dijkstra shortest path algorithm
     * @param origin the origin serving as the root node of a bush
     * @param edgesCosts costs of all edges of the graph
     * @return the distances of all nodes to the origin serving as the root node of a bush using the Dijkstra shortest path algorithm
     * @throws IllegalArgumentException if the number of edges does not correspond to the number of costs of all edges
     */
    protected double[] determineNodesDistancesDijkstra(TmZone origin, double[] edgesCosts) throws IllegalArgumentException{
        if (edgesCosts.length != edges.size()) {
            throw new IllegalArgumentException("The edges costs count must correspond to the edges count.");
        }

        double[] nodesDistances = new double[nodes.size()];
        Arrays.fill(nodesDistances, Double.POSITIVE_INFINITY);
        TmNode node = origin.getNode();
        TmPriorityQueueElement<Double, TmNode> element = new TmPriorityQueueElement<>(0.0, node);
        PriorityQueue<TmPriorityQueueElement<Double, TmNode>> priorityQueue = new PriorityQueue<>();
        nodesDistances[node.getIndex()] = 0.0;
        priorityQueue.offer(element);

        while (!priorityQueue.isEmpty()) {
            element = priorityQueue.poll();
            node = element.getValue();

            for (TmEdge edge: node.getOutgoingEdges()) {
                if (nodesDistances[node.getIndex()] + edgesCosts[edge.getIndex()] < nodesDistances[edge.getEndNode().getIndex()]) {
                    nodesDistances[edge.getEndNode().getIndex()] = nodesDistances[node.getIndex()] + edgesCosts[edge.getIndex()];
                    priorityQueue.offer(new TmPriorityQueueElement<>(nodesDistances[edge.getEndNode().getIndex()], edge.getEndNode()));
                }
            }
        }

        return nodesDistances;
    }

    /**
     * Finds and returns the minimal (on <code>MINIMAL_COST_TREE</code> index) and maximal (on <code>MAXIMAL_COST_TREE</code> index) cost trees for the specified bush and costs of all edges
     * @param bush the bush, for which the minimal and maximal cost trees shall be found
     * @param edgesCosts the costs of all edges
     * @param maximalCostAllEdges determines, whether all edges (<code>true</code>) or only edges with non-zero flows (<code>false</code>) shall be considered during the construction of the maximal cost tree
     * @return the minimal (on <code>MINIMAL_COST_TREE</code> index) and maximal (on <code>MAXIMAL_COST_TREE</code> index) cost trees for the specified bush and costs of all edges
     */
    public TmBTree[] findMinimalMaximalCostTrees(TmBBush bush, double[] edgesCosts, boolean maximalCostAllEdges) {
        TmBTree minimalCostTree = new TmBTree(bush.getOrigin(), nodes.size(), Double.POSITIVE_INFINITY);
        TmBTree maximalCostTree = new TmBTree(bush.getOrigin(), nodes.size(), Double.NEGATIVE_INFINITY);

        Queue<TmPairElement<TmNode, TmEdge>> queue = new LinkedList<>();
        boolean[] edgesMask = Arrays.copyOf(bush.getEdgesMask(), bush.getEdgesMask().length);
        TmNode node = bush.getOrigin().getNode();
        TmPairElement<TmNode, TmEdge> pair = new TmPairElement<>(node, null);
        queue.offer(pair);
        minimalCostTree.setDistance(node.getIndex(), 0.0);
        maximalCostTree.setDistance(node.getIndex(), 0.0);
        double distance = 0.0;

        while (!queue.isEmpty()) {
            pair = queue.poll();

            for (TmEdge edge: pair.getFirst().getOutgoingEdges()) {
                if (edgesMask[edge.getIndex()]) {
                    //Maximal cost tree
                    if (bush.getEdgeFlow(edge.getIndex()) != 0.0 || maximalCostAllEdges) {
                        distance = maximalCostTree.getDistance(pair.getFirst().getIndex()) + edgesCosts[edge.getIndex()];
                        if (maximalCostTree.getDistance(edge.getEndNode().getIndex()) < distance) { //Higher cost path found
                            maximalCostTree.setDistance(edge.getEndNode().getIndex(), distance);
                            maximalCostTree.setPreviousEdge(edge.getEndNode().getIndex(), edge);
                        }
                    }

                    //Minimal cost tree
                    distance = minimalCostTree.getDistance(pair.getFirst().getIndex()) + edgesCosts[edge.getIndex()];

                    if (minimalCostTree.getDistance(edge.getEndNode().getIndex()) > distance) {
                        minimalCostTree.setDistance(edge.getEndNode().getIndex(), distance);
                        minimalCostTree.setPreviousEdge(edge.getEndNode().getIndex(), edge);
                    }

                    edgesMask[edge.getIndex()] = false;
                    if (!existsIncomingEdge(edge.getEndNode(), edgesMask)) {
                        queue.offer(new TmPairElement<>(edge.getEndNode(), edge));
                    }
                }
            }
        }

        return new TmBTree[] {minimalCostTree, maximalCostTree};
    }

    /**
     * Returns <code>true</code>, if there is an incoming edge for the specified node and bush edges. Returns <code>false</code> otherwise
     * @param node the node, for which the existence of the incoming edge shall be returned
     * @param edgesMask determines which edges of the graph are in the bush
     * @return <code>true</code>, if there is an incoming edge for the specified node and bush edges. Returns <code>false</code> otherwise
     */
    protected boolean existsIncomingEdge(TmNode node, boolean[] edgesMask) {
        boolean exists = false;

        for (TmEdge edge: node.getIncomingEdges()) {
            if (edgesMask[edge.getIndex()]) {
                exists = true;
                break;
            }
        }

        return exists;
    }

    /**
     * Determines and returns the immediate previous edges of all nodes on the paths from this nodes to the specified origin serving as the root node of a bush
     * @param origin the origin serving as the root node of a bush
     * @param edgesCosts costs of all edges of the graph
     * @return the immediate previous edges of all nodes on the paths from this nodes to the specified origin serving as the root node of a bush
     * @throws IllegalArgumentException if the number of edges does not correspond to the number of costs of all edges
     */
    public TmEdge[] determineNodesPreviousEdges(TmZone origin, double[] edgesCosts) throws IllegalArgumentException {
        return determineNodesPreviousEdgesDijkstra(origin, edgesCosts);
    }

    /**
     * Determines and returns the immediate previous edges of all nodes on the paths from this nodes to the specified origin serving as the root node of a bush using the Dijkstra shortest path algorithm
     * @param origin the origin serving as the root node of a bush
     * @param edgesCosts costs of all edges of the graph
     * @return the immediate previous edges of all nodes on the paths from this nodes to the specified origin serving as the root node of a bush
     * @throws IllegalArgumentException if the number of edges does not correspond to the number of costs of all edges
     */
    protected TmEdge[] determineNodesPreviousEdgesDijkstra(TmZone origin, double[] edgesCosts) throws IllegalArgumentException{
        if (edgesCosts.length != edges.size()) {
            throw new IllegalArgumentException("The edges costs count must correspond to the edges count.");
        }

        double[] nodesDistances = new double[nodes.size()];
        TmEdge[] nodesPreviousEdges = new TmEdge[nodes.size()];
        Arrays.fill(nodesDistances, Double.POSITIVE_INFINITY);
        TmNode node = origin.getNode();
        TmPriorityQueueElement<Double, TmNode> element = new TmPriorityQueueElement<>(0.0, node);
        PriorityQueue<TmPriorityQueueElement<Double, TmNode>> priorityQueue = new PriorityQueue<>();
        nodesDistances[node.getIndex()] = 0.0;
        priorityQueue.offer(element);

        while (!priorityQueue.isEmpty()) {
            element = priorityQueue.poll();
            node = element.getValue();

            for (TmEdge edge: node.getOutgoingEdges()) {
                if (nodesDistances[node.getIndex()] + edgesCosts[edge.getIndex()] < nodesDistances[edge.getEndNode().getIndex()]) {
                    nodesDistances[edge.getEndNode().getIndex()] = nodesDistances[node.getIndex()] + edgesCosts[edge.getIndex()];
                    nodesPreviousEdges[edge.getEndNode().getIndex()] = edge;
                    priorityQueue.offer(new TmPriorityQueueElement<>(nodesDistances[edge.getEndNode().getIndex()], edge.getEndNode()));
                }
            }
        }

        return nodesPreviousEdges;
    }

    /**
     * Finds are returns the path from the destination representing a node of the graph to the root node using the immediate previous edges of all nodes
     * @param destination the destination representing a node of the graph
     * @param nodesPreviousEdges the immediate previous edges of all nodes
     * @return the path from the destination representing a node of the graph to the root node using the immediate previous edges of all nodes
     */
    public TmEdge[] extractPath(TmZone destination, TmEdge[] nodesPreviousEdges) {
        List<TmEdge> pathList = new ArrayList<>();

        TmEdge edge = nodesPreviousEdges[destination.getNode().getIndex()];
        while (edge != null) {
            pathList.add(edge);
            edge = nodesPreviousEdges[edge.getStartNode().getIndex()];
        }

        TmEdge[] path = new TmEdge[pathList.size()];
        for (int i = 0; i < path.length; i++) {
            path[i] = pathList.get(path.length - i - 1);
        }
        return path;
    }

    //Getters and setters

    /**
     * Returns the name of the model, which is represented by this graph
     * @return the name of the model, which is represented by this graph
     */
    public String getModelName() {
        return modelName;
    }



    /**
     * Sets the name of the model, which is represented by this graph
     * @param modelName the new name of model, which is represented by this graph
     */
    public void setModelName(String modelName) {
        this.modelName = modelName;
    }



    /**
     * Adds the specified edge to this graph
     * @param edge an edge, which shall be added to this graph
     */
    public void addEdge(TmEdge edge) {
        edges.add(edge);

        if (edge.getId() < minimalEdgeId) {
            minimalEdgeId = edge.getId();
        }
        if (edge.getId() > maximalEdgeId) {
            maximalEdgeId = edge.getId();
        }
    }

    /**
     * Returns all the edges of this graph
     * @return all the edges of this graph
     */
    public List<TmEdge> getEdges() {
        return edges;
    }

    /**
     * Adds the specified node to this graph
     * @param node a node, which shall be added to this graph
     */
    public void addNode(TmNode node) {
        nodes.add(node);

        if (node.getId() < minimalNodeId) {
            minimalNodeId = node.getId();
        }
        if (node.getId() > maximalNodeId) {
            maximalNodeId = node.getId();
        }
    }

    /**
     * Returns all the nodes of this graph
     * @return all the nodes of this graph
     */
    public List<TmNode> getNodes() {
        return nodes;
    }
}