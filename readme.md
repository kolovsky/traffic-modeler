Traffic Modeler
=====================

Homepage: [https://trafficmodeller.com/](https://trafficmodeller.com/)

Tool for transport modelling. The tool implement static traffic assignment (user equilibrium, traffic assignment problem) and effective methods for Origin-Destination matrix estimation.

Traffic Assignment
------------------
Library provides following methods for the traffic assignment:

* All-or-nothing (AON)
* User equilibrium (UE) - You can choose from two following algorithms:
    * Path based algorithm [Jayakrishnan 1994]
    * B algorithm [Dial 2006, Nie 2010]

[Nie 2010] NIE, Yu Marco. A class of bush-based algorithms for the traffic assignment problem. Transportation Research Part B: Methodological, 2010, 44.1: 73-89.

[Jayakrishnan 1994] Jayakrishnan, R., et al. "A faster path-based algorithm for traffic assignment." University of California Transportation Center (1994).

[Dial 2006] Dial, Robert B. "A path-based user-equilibrium traffic assignment algorithm that obviates path storage and enumeration." Transportation Research Part B: Methodological 40.10 (2006): 917-936.

Origin-destination matrix estimation
------------------------------------
Following methods are provided:

* Gravity model
* Estimation using traffic count (for AON and UE)

