﻿-- version: 1.0.1
-- author: Frantisek Kolovsky

CREATE SCHEMA tm_pilsen_new;
SET search_path = tm_pilsen_new;

-- drop existing tables
DROP TABLE IF EXISTS edge;
DROP TABLE IF EXISTS odm;
DROP TABLE IF EXISTS zone;
DROP TABLE IF EXISTS cache;
DROP TABLE IF EXISTS node;

-- create new tables
CREATE TABLE node
(
 node_id int primary key,
 geometry public.geometry(Point, 4326) NOT NULL
);

CREATE TABLE edge
(
  edge_id serial PRIMARY KEY,
  source int NOT NULL REFERENCES node(node_id),
  target int NOT NULL REFERENCES node(node_id),
  capacity real NOT NULL,
  cost real NOT NULL,
  isvalid boolean NOT NULL,
  turn_restriction text NOT NULL,
  geometry public.geometry(LineString,4326) NOT NULL
);

CREATE TABLE zone
(
  zone_id int PRIMARY KEY,
  node_id int NOT NULL,
  trips real NOT NULL,
  geometry public.geometry(Point,4326) NOT NULL
);

CREATE TABLE odm
(
  source int REFERENCES zone(zone_id) NOT NULL,
  source_node int NOT NULL,
  target int REFERENCES zone(zone_id) NOT NULL,
  target_node int NOT NULL,
  flow real NOT NULL,
  PRIMARY KEY (source, target)
);

CREATE TABLE cache
(
  name character varying(50) PRIMARY KEY,
  time_stamp timestamp without time zone DEFAULT now(),
  config json NOT NULL,
  result json NOT NULL
);

SET search_path = public;
